<?php

namespace Astro_Element\Elementor;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;

use Astro_Element\Elementor_Base;
use Astro_Element\Helper;
use Astro_Element\HTML;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Heading extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-heading';
    }

    public function get_title()
    {
        return __('Heading', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-headline';

    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();

    }


  
    protected function render()
    {
        $settings = $this->get_settings_for_display();

       // Header
        echo HTML::header_block(array(
            'class' => 'ael-header-block--' . $settings['header_style'],
            'title' => $settings['header_title'],
            'nav' => false,
            'id' => 'header-' . $this->get_id(),
        ));

    }
}
