<?php

namespace Astro_Element\Elementor;

use Astro_Element\Elementor_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Menu extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-nav-menu';
    }

    public function get_title()
    {
        return __('Nav Menu', 'astro-element');
    }

    public function get_icon()
    {
        return 'eicon-nav-menu';
    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    protected function _register_controls()
    {

        $this->setting_option();
        $this->general_style();

    }

    private function get_available_menus()
    {
        $menus = wp_get_nav_menus();

        $options = [];

        foreach ($menus as $menu) {
            $options[$menu->slug] = $menu->name;
        }

        return $options;
    }

    protected function setting_option()
    {
        $this->start_controls_section(
            'content_option',
            [
                'label' => __('Options', 'astro-element'),
            ]
        );

        $menus = $this->get_available_menus();

        if (!empty($menus)) {
            $this->add_control(
                'menu',
                [
                    'label' => __('Menu', 'elementor-pro'),
                    'type' => Controls_Manager::SELECT,
                    'options' => $menus,
                    'default' => array_keys($menus)[0],
                    'save_default' => true,
                    'separator' => 'after',
                    'description' => sprintf(__('Go to the <a href="%s" target="_blank">Menus screen</a> to manage your menus.', 'elementor-pro'), admin_url('nav-menus.php')),
                ]
            );
        } else {
            $this->add_control(
                'menu',
                [
                    'type' => Controls_Manager::RAW_HTML,
                    'raw' => sprintf(__('<strong>There are no menus in your site.</strong><br>Go to the <a href="%s" target="_blank">Menus screen</a> to create one.', 'elementor-pro'), admin_url('nav-menus.php?action=edit&menu=0')),
                    'separator' => 'after',
                    'content_classes' => 'elementor-panel-alert elementor-panel-alert-info',
                ]
            );
        }
        $this->add_control(
            'menu_layout',
            [
                'label' => __('Layout', 'elementor-pro'),
                'type' => Controls_Manager::SELECT,
                'default' => 'vertical',
                'options' => [
                    'vertical' => __('Vertical', 'elementor-pro'),
                    'horizonal' => __('Horizonal', 'elementor-pro'),
                ],

            ]
        );

        
        $this->end_controls_section();
    }

    public function general_style()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'general_typography',
                'selector' => '{{WRAPPER}} .ael-menu a',
            ]
        );
        $this->add_control(
            'menu_align',
            [
                'label' => __('Align', 'elementor-pro'),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => false,
                'default' => 'center',
                'options' => [
                    'flex-left' => [
                        'title' => __('Left', 'elementor-pro'),
                        'icon' => 'eicon-h-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'elementor-pro'),
                        'icon' => 'eicon-h-align-center',
                    ],
                    'flex-right' => [
                        'title' => __('Right', 'elementor-pro'),
                        'icon' => 'eicon-h-align-right',
                    ],
                ],
                 'selectors' => [
                    '{{WRAPPER}} .ael-menu' => 'justify-content: {{VALUE}};',
                ],
            ]
        );
        
         /** background tabs start */
        $this->start_controls_tabs('general_background_tabs');

        $this->start_controls_tab(
            'menu_normal',
            [
                'label' => __('Normal', 'astro-element'),
            ]
        );
         $this->add_control(
            'menu_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-menu a' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->end_controls_tab();

          $this->start_controls_tab(
            'menu_hover',
            [
                'label' => __('Hover', 'astro-element'),
            ]
        );
         $this->add_control(
            'menu_color_hover',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-menu a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->end_controls_tab();


          $this->start_controls_tab(
            'menu_active',
            [
                'label' => __('Active', 'astro-element'),
            ]
        );
         $this->add_control(
            'menu_color_active',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-menu a:active' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();
   
        if ($settings['menu']) {
            $args = array(
                'container' => 'div',
                'container_class' => "ael-menu ael-menu--{$settings['menu_layout']}",
                'menu_class' => 'ael-menu__container',
                'menu' => $settings['menu'],
            );

            wp_nav_menu($args);
        }

    }
}
