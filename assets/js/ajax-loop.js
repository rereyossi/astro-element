/*=================================================
*  AJAX CALL
// =================================================== */

jQuery(document).ready(function($) {
  
  var astro_button = $(".js-astro-loop-load");

  astro_button.on("click", function(event) {
    event.preventDefault();

    var block_id = $(this).data("triger-id");
    var element = $("#" + block_id);
    var settings = eval($(this).data("triger-id"));
    var current_page = element.attr("data-page");
    var max_num_pages = element.attr("data-max-paged");
    var trigger = $('[data-triger-id="' + block_id + '"]');
    var spinner = $(".js-" + block_id + "-spinner");


    if ($(this).hasClass("prev")) {
      var page_action = parseInt(current_page) - 1;
    } else {
      var page_action = parseInt(current_page) + 1;
    }

    var data = {
      action: "astro_ajax_loop_result",
      page: page_action,
      settings: settings,
      query: astro_ajax_loop.posts,
      block: block_id,
      check_nonce: astro_ajax_loop.check_nonce,
    };

    jQuery.ajax({
      url: astro_ajax_loop.ajaxurl,
      type: "post",
      dataType: "html",
      data: data,
      beforeSend: function(response) {
        trigger.hide();
        spinner.show();
        element.find(".flex-item").addClass("is-show js-page-1");
        
        element.addClass('rt-loading');
 
      },
      success: function(response) {


        element.removeClass("rt-loading");

        var next_page = parseInt(current_page) + 1;
        var prev_page = parseInt(current_page) - 1;

        if (response != 0) {
          // check layout masonry
          if (element.hasClass("js-astro-masonry")) {
            var content = $(response);

            // add new content 
            var $grid = element.append(content)
              .masonry('appended', content);

            // layout Masonry after each image loads
            $grid.imagesLoaded().progress(function () {
              $grid.masonry('layout')
                .masonry({ horizontalOrder: true });
            });
            
          } else {
            element.append(response);
          }

          // add new current page
          if ($(this).hasClass("prev")) {
            element.attr("data-page", prev_page);
          } else {
            element.attr("data-page", next_page);
          }

          // add animation
          element
            .find(".flex-item:not(.is-show)")
            .velocity("transition.expandIn", {
              duration: 300
            })
            .addClass("is-show js-page-" + next_page);
          
          // hide and show button load more
          setTimeout(function() {
            trigger.show();
            spinner.hide();
          }, 500);

          // remove button read more if last page
          if (next_page >= max_num_pages) {
            trigger.remove();
          }
        } else {
          console.log("gagal");
        }
      }
    });
  });
});
