<div class="flex-item">

    <div class="ael-term-box">

        <?php if(!empty($thumbnail_id) && $settings['image_size'] !== 'none'): ?>
        <a href="<?php echo get_term_link($term->term_id)?>">
            <div class="ael-term-box__thumbnail rt-img rt-img--full">
                <?php echo  wp_get_attachment_image($thumbnail_id, $settings['image_size']); ?>
            </div>
        </a>
        <?php endif ?>

        <div class="ael-term-box__inner">
            
            <div class="ael-term-box__body">

                <?php if($settings['term_title'] == 'yes'):?>
                <h4 class="ael-term-box__title">
                    <a href="<?php echo get_term_link($term->term_id)?>"><?php echo $term->name; ?></a>
                </h4>
                <?php endif ?>

                <?php if(!empty($term->description) && $settings['term_desc'] == 'yes'): ?>
                    <div class="ael-term-box__content"><?php echo $term->description; ?></div>
                <?php endif ?>

                <?php if($settings['term_count'] == 'yes'): ?>
                    <div class="ael-term-box__count"><?php echo $term->count .' '.$settings['term_count_text'] ?></div>
                <?php endif ?>

            </div>

        </div>
    </div>
    
</div>

