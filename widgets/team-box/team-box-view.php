
<?php 
$style = $settings['social_style'];

$url_target = ($settings['is_external'] == 'yes') ? ' target="_blank"' : '';
$url_nofollow = ($settings['nofollow'] == 'yes') ? ' rel="nofollow"' : '';

?>
<div class="flex-item mb-0">
    <div id="team-<?php echo $this->get_id()?>" class="ael-team">

        <div class="ael-team__thumbnail rt-img rt-img--full">
        <?php echo astro_get_attachment_image($team['image'], $settings['image_size']) ?>
        </div>
        
        <div class="ael-team__body">
            <h4 class="ael-team__name"><?php echo $team['name'] ?></h4>
            <span class="ael-team__position"><?php echo $team['position'] ?></span>
            <div class="ael-team__content"><?php echo $team['content'] ?></div>

            <div class="rt-socmed <?php echo 'rt-socmed--'.$style?>">
            
                <?php if($team['link_facebook']['url']): ?>
                <a href="<?php echo $team['link_facebook']['url']?>" class="rt-socmed__item facebook" <?php $url_target.' '.$url_nofollow?>>
                    <i class="fa fa-facebook"></i>
                </a>
                <?php endif ?>

                <?php if($team['link_youtube']['url']): ?>
                <a href="<?php echo $team['link_youtube']['url']?>" class="rt-socmed__item youtube" <?php $url_target.' '.$url_nofollow?>>
                    <i class="fa fa-youtube"></i>
                </a>
                <?php endif ?>

                <?php if($team['link_instagram']['url']): ?>
                <a href="<?php echo $team['link_instagram']['url']?>" class="rt-socmed__item instagram" <?php $url_target.' '.$url_nofollow?>>
                    <i class="fa fa-instagram"></i>
                </a>
                <?php endif ?>

                <?php if($team['link_linkedin']['url']): ?>
                <a href="<?php echo $team['link_linkedin']['url']?>" class="rt-socmed__item linkedin" <?php $url_target.' '.$url_nofollow?>>
                    <i class="fa fa-linkedin"></i>
                </a>
                <?php endif ?>

                <?php if($team['link_email']['url']): ?>
                <a href="<?php echo 'mailto:'.$team['link_email']['url']?>" class="rt-socmed__item email" <?php $url_target.' '.$url_nofollow?>>
                    <i class="fa fa-envelope"></i>
                </a>
                <?php endif ?>

            </div>

        </div>

    </div>
</div>