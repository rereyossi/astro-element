<?php
/*=================================================;
/* GET PAGE ADMIN
/*================================================= */
if (!function_exists('astro_admin_get_page')) {
    function astro_admin_get_page()
    {
        return !empty($_GET['page']) ? $_GET['page'] : '';
    }
}

if (!function_exists('astro_admin_theme_page')) {
    function astro_admin_theme_page()
    {
        return array(
            'theme-dashboard',
            'theme-option',
            'theme-panel',
            'theme-license',
            'theme-pro',
            'theme-contact',
            'tgmpa-install-plugins',
            'pt-one-click-demo-import',
        );
    }
}

/**
 * get admin page url
 *
 * @param [type] $url
 * @return url page
 */
if (!function_exists('astro_admin_page_url')) {
    function astro_admin_page_url($url)
    {
        return admin_url() . "admin.php?page={$url}";
    }
}

/*=================================================;
/* HANDLE OPTION
/*================================================= */
/**
 * Check if user is only running the product’s Premium Version code
 *
 * @return true on localhost or user have valid license
 */
function ael_is_local()
{
    $site = $_SERVER['SERVER_NAME'];

    $local = array(
        'localhost',
        '127.0.0.1',
        '10.0.0.0/8',
        '172.16.0.0/12',
        '192.168.0.0/16',
        '*.dev',
        '.*local',
        'dev.*',
        'staging.*',
    );

    if (in_array($site, $local)) {
        return false;
    }
}

function ael_is_premium()
{
    if (ael_is_local()) {
        return true;
    } else {
        return (get_option(ASTRO_ELEMENT_SLUG. '_status') == 'active') ? true : false;
    }

}
/**
 * code running premium active or not
 *
 * @return void
 */
function ael_is_premium_plan()
{
    return true;
}

function ael_is_free()
{
    if (ael_is_premium()) {
        return false;
    } else {
        return true;
    }
}

/**
 * Check if the user is on the free plan of the product and not want to active premium.
 */
function ael_is_free_plan()
{
    if (ael_is_premium_plan()) {
        return false;
    } else {
        return true;
    }

}

/**
 * code not running because, code for future plan
 *
 * @return void
 */
function ael_is_feature()
{
    return false;
}

