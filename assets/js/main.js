jQuery(document).ready(function ($) {
  /*=================================================;
  /* HELPER FUNCTION
  /*================================================= */
  $.fn.hasData = function (key) {
    return typeof $(this).data(key) != "undefined";
  };

  $.fn.getData = function (key) {
    var data = false;

    if ($(this).data(key) != "undefined") {
      var data = $(this).data(key);
    }

    return data;
  };
  /*=================================================;
  /* SLIDER
  /*================================================= */


  var slider = function (element) {
    $(".js-astro-slider").each(function (index) {
      // get form slider
      var slider = $("#" + $(this).attr("id"));
      var slider_main = $("#" + $(this).attr("id")).find(".ael-slider__main");
      var slider_thumb = $("#" + $(this).attr("id")).find(".ael-slider__group"); // disable class if not infinity slider

      if (slider.hasData("loop")) {
        slider.find(".js-astro-slider-prev").addClass("is-disable");
      }

      slider_main.owlCarousel({
        loop: slider.getData("loop"),
        animateOut: slider.getData("animateout"),
        animateIn: slider.getData("animatein"),
        autoplay: slider.getData("autoplay"),
        lazyLoad: slider.getData("lazyload"),
        center: slider.getData("center"),
        margin: slider.getData("gap"),
        nav: slider.getData("nav"),
        dots: slider.getData("pagination"),
        stagePadding: slider.getData("padding"),
        autoplayHoverPause: slider.getData("autoplayhoverpause"),
        autoplaySpeed: slider.getData("autoplayspeed"),
        URLhashListener: slider.getData("urlhashListener"),
        startPosition: "URLHash",
        navText: ["<i class='" + slider.getData("nav-icon-left") + "'></i>", "<i class='" + slider.getData("nav-icon-right") + "'></i>"],
        responsive: {
          0: {
            items: slider.getData("items-sm")
          },
          720: {
            items: slider.getData("items-md")
          },
          960: {
            items: slider.getData("items-lg")
          }
        },
        onTranslated: function (element) {
          slider.find(".ael-slider__nav a").removeClass("is-disable"); // disable nav header if first item or last item

          if (slider.find(".owl-item").first().hasClass("active")) {
            slider.find(".js-astro-slider-prev").addClass("is-disable");
            slider.find(".js-astro-slider-next").removeClass("is-disable");
          }

          if (slider.find(".owl-item").last().hasClass("active")) {
            slider.find(".js-astro-slider-prev").removeClass("is-disable");
            slider.find(".js-astro-slider-next").addClass("is-disable");
          }
        },
        onInitialized: function (element) {
          // hidden nav if only 1 page
          var pages = element.page.size; // Number of pages

          var items = element.item.count; // Number of items

          if (pages == items) {
            slider.find(".ael-slider__nav").hide();
          } else {
            slider.find(".ael-slider__nav").show();
          }
        }
      });
      /**
       * CUSTOM NAVIGATION ON HEADER
       */

      slider.find(".js-astro-slider-prev").click(function (event) {
        slider.find(".ael-slider__main").trigger("prev.owl.carousel");
      });
      slider.find(".js-astro-slider-next").click(function (event) {
        slider.find(".ael-slider__main").trigger("next.owl.carousel");
      });
      /** end each */
    });
  };
  /*=================================================;
  /* SLIDER SYNC
  /*================================================= */


  var slider_sync = function (element) {
    $(".js-astro-slider-sync").each(function (index) {
      // get form slider
      var slider = $("#" + $(this).attr("id"));
      var slider_main = $("#" + $(this).attr("id")).find(".ael-slider__main");
      var slider_thumb = $("#" + $(this).attr("id")).find(".ael-slider__group");
      slider_main.owlCarousel({
        items: 1,
        loop: true,
        nav: slider.getData('nav'),
        dots: false,
        navText: ["<i class='" + slider.getData("nav-icon-left") + "'></i>", "<i class='" + slider.getData("nav-icon-right") + "'></i>"]
      }).on("changed.owl.carousel", syncPosition);
      /**
       * SLIDER THUMBNAIL
       */

      var syncedSecondary = true; // set margin button featured image

      slider_main.css("margin-bottom", slider.getData("gap") + "px");
      slider_thumb.on("initialized.owl.carousel", function () {
        slider_thumb.find(".owl-item").eq(0).addClass("is-current");
      }).owlCarousel({
        dots: false,
        nav: false,
        smartSpeed: 200,
        slideSpeed: 500,
        margin: slider.getData("gap"),
        responsiveRefreshRate: 100,
        responsive: {
          0: {
            items: slider.getData("items-sm")
          },
          720: {
            items: slider.getData("items-md")
          },
          960: {
            items: slider.getData("items-lg")
          }
        }
      }).on("changed.owl.carousel", syncPosition2);

      function syncPosition(el) {
        //if you set loop to false, you have to restore this next line
        //var current = el.item.index;
        //if you disable loop you have to comment this block
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

        if (current < 0) {
          current = count;
        }

        if (current > count) {
          current = 0;
        } //end block


        slider_thumb.find(".owl-item").removeClass("is-current").eq(current).addClass("is-current");
        var onscreen = slider_thumb.find(".owl-item.active").length - 1;
        var start = slider_thumb.find(".owl-item.active").first().index();
        var end = slider_thumb.find(".owl-item.active").last().index();

        if (current > end) {
          slider_thumb.data("owl.carousel").to(current, 100, true);
        }

        if (current < start) {
          slider_thumb.data("owl.carousel").to(current - onscreen, 100, true);
        }
      }

      function syncPosition2(el) {
        if (syncedSecondary) {
          var number = el.item.index;
          slider_main.data("owl.carousel").to(number, 100, true);
        }
      }

      slider_thumb.on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = $(this).index();
        slider_main.data("owl.carousel").to(number, 300, true);
      });
      /** end each */
    });
  };
  /*=================================================;
  /* SLIDER ONLY MOBILE
  /*================================================= */


  var slider_mobile = function () {
    var slider = $('.js-astro-slider-mobile');
    var slider_options = {
      items: 1,
      loop: true,
      margin: 1,
      nav: false,
      stagePadding: 15,
      lazyLoad: true
    };

    if ($(window).width() <= 480) {
      var slider_active = slider.owlCarousel(slider_options).addClass('owl-carousel');
    } else {
      slider.removeClass('owl-carousel');
    }

    $(window).resize(function () {
      if ($(window).width() <= 480) {
        var slider_active = slider.owlCarousel(slider_options);
        slider.addClass('owl-carousel');
      } else {
        slider.addClass('off').trigger('destroy.owl.carousel');
        slider.find('.owl-stage-outer').children(':eq(0)').unwrap();
        slider.removeClass('owl-carousel');
      }
    });
  };
  /*=================================================
   *  TYPED JS
   =================================================== */


  var typed = function () {
    //
    var element = $('.js-astro-headline');
    element.each(function () {
      var defaults = {
        loop: true,
        speed: 40
      };
      var meta = $(this).data();
      var options = $.extend(defaults, meta);
      var get_id = $(this).attr('id');
      var get_data = '#' + get_id + ' .ael-headline__data';
      var get_value = '#' + get_id + ' .ael-headline__value';
      var typed = new Typed(get_value, {
        stringsElement: get_data,
        loop: options.loop,
        typeSpeed: options.speed,
        showCursor: false,
        cursorChar: '|'
      });
    });
  };
  /* =================================================
  *  MASONRY
  * =================================================== */


  var masonry = function (element) {
    // init Masonry
    var $grid = $(".js-astro-masonry").masonry({
      columnWidth: ".flex-item",
      itemSelector: ".flex-item"
    }); // layout Masonry after each image loads

    $grid.imagesLoaded().progress(function () {
      $grid.masonry('layout').masonry({
        horizontalOrder: true
      });
      ;
    });
  };
  /*=================================================;
  /* COUNTDOWN
  /*================================================= */


  var countdown = function () {
    var element = $('.js-countdown');
    element.each(function (indexInArray, valueOfElement) {
      // get id each element countdown
      var countdown = $('#' + $(this).attr('id'));
      var get_date = countdown.getData('date');
      countdown.countdown(countdown.getData('date'), function (event) {
        $(this).find('.js-countdown_day').html(event.strftime('%d'));
        $(this).find('.js-countdown_hour').html(event.strftime('%H'));
        $(this).find('.js-countdown_minute').html(event.strftime('%M'));
        $(this).find('.js-countdown_second').html(event.strftime('%S'));
      });
    });
  };
  /*=================================================
  *  ELEMENTOR REGISTER
  =================================================== */


  var elementor_frontend_hook = function () {
    elementorFrontend.hooks.addAction('frontend/element_ready/astro-posts.default', masonry);
    elementorFrontend.hooks.addAction('frontend/element_ready/astro-posts.default', slider);
    elementorFrontend.hooks.addAction('frontend/element_ready/astro-posts-list.default', slider);
    elementorFrontend.hooks.addAction('frontend/element_ready/astro-posts-tiles.default', masonry);
    elementorFrontend.hooks.addAction('frontend/element_ready/astro-posts-tiles.default', slider);
    elementorFrontend.hooks.addAction('frontend/element_ready/astro-posts-smart-tiles.default', masonry);
    elementorFrontend.hooks.addAction('frontend/element_ready/astro-posts-smart-tiles.default', slider_mobile);
    elementorFrontend.hooks.addAction("frontend/element_ready/astro-posts-smart-tiles.default", masonry);
    elementorFrontend.hooks.addAction('frontend/element_ready/astro-posts-slider.default', slider);
    elementorFrontend.hooks.addAction("frontend/element_ready/astro-posts-slider-thumb.default", slider_sync);
    elementorFrontend.hooks.addAction('frontend/element_ready/astro-portfolio.default', slider);
    elementorFrontend.hooks.addAction("frontend/element_ready/astro-countdown.default", countdown);
    elementorFrontend.hooks.addAction("frontend/element_ready/astro-slider.default", slider);
    elementorFrontend.hooks.addAction("frontend/element_ready/astro-carousel.default", slider);
    elementorFrontend.hooks.addAction("frontend/element_ready/astro-headline.default", typed);
    elementorFrontend.hooks.addAction('frontend/element_ready/astro-content-box.default', masonry);
    elementorFrontend.hooks.addAction('frontend/element_ready/astro-content-box.default', slider);
    elementorFrontend.hooks.addAction('frontend/element_ready/astro-product-cat.default', slider);
  };

  if ($('body').hasClass('elementor-editor-active')) {
    elementor_frontend_hook();
  } else {
    $(window).on('elementor/frontend/init', function () {
      elementor_frontend_hook();
    });
  } // end jquery ready

});