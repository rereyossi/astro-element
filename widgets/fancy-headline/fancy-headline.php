<?php

namespace Astro_Element\Elementor;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Fancy_Headline extends \Astro_Element\Elementor_Base
{

    public function get_name()
    {
        return 'astro-headline';
    }

    public function get_title()
    {
        return __('Headline', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-headline';

    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    protected function _register_controls()
    {

        /*=================================================
         *  SETTING: HEADLINE
        /*================================================= */
        $this->start_controls_section(
            'setting_text',
            [
                'label' => __('Text Headline', 'astro-element'),
            ]
        );
        $this->add_control(
            'before_headline',
            [
                'label'   => __('Text Before Headline', 'astro-element'),
                'type'    => Controls_Manager::TEXTAREA,
                'default' => 'Astro Element',
            ]
        );

        $this->add_control(
            'headline',
            [
                'label'       => 'Headline',
                'description' => 'Set comma seprated headline (headline 1, headline 2, headline 3 etc.)',
                'type'        => Controls_Manager::TEXTAREA,
                'default'     => 'Awesome, Pretty, light Weight',
            ]
        );

        $this->add_control(
            'after_headline',
            [
                'label'   => __('Text After Headline', 'astro-element'),
                'type'    => Controls_Manager::TEXTAREA,
                'default' => 'Add ons for Elementor',
            ]
        );

        $this->end_controls_section();
        /*=================================================
         *  SECTION: SETTING
        /*================================================= */
        $this->start_controls_section(
            'section_setting',
            [
                'label' => __('Settings', 'astro-element'),
            ]
        );
        $this->add_control(
            'loop',
            [
                'label'        => __('Loop', 'astro-element'),
                'type'         => Controls_Manager::SWITCHER,
                'default'      => true,
                'label_on'     => __('On', 'your-plugin'),
                'label_off'    => __('Off', 'your-plugin'),
                'return_value' => true,
            ]
        );

        $this->add_control(
            'speed',
            [
                'label'   => __('Speed', 'astro-element'),
                'type'    => Controls_Manager::NUMBER,
                'default' => 40,
            ]
        );
        $this->end_controls_section();

        /*=================================================
         *  STYLE: TEXT
        /*================================================= */
        $this->start_controls_section(
            'style_text',
            [
                'label' => __('Text', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_control(
            'text_color',
            [
                'label'     => __('Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-headline__wrapper' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'text_typo',
                'selector' => '{{WRAPPER}} .ael-headline__wrapper',
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'     => 'text_background',
                'selector' => '{{WRAPPER}}  .ael-headline__regular',
            ]
        );

        $this->add_responsive_control(
            'text_indent',
            [
                'label'     => __('Text Indent', 'elementor'),
                'type'      => Controls_Manager::SLIDER,
                'range'     => [
                    'px' => [
                        'max' => 50,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .ael-headline__regular' => 'margin-right: {{SIZE}}{{UNIT}};',
                    '{{WRAPPER}} .ael-headline__value'   => 'margin-right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'text_align',
            [
                'label'     => __('Text Alignment', 'astro-element'),
                'type'      => Controls_Manager::CHOOSE,
                'options'   => [
                    'left'   => [
                        'title' => __('Left', 'astro-element'),
                        'icon'  => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro-element'),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'right'  => [
                        'title' => __('Right', 'astro-element'),
                        'icon'  => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .ael-headline__wrapper' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

        /*=================================================
         *  STYLE: HEADLINE
        /*================================================= */
        $this->start_controls_section(
            'style_headline',
            [
                'label' => __('Headline', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_control(
            'headline_color',
            [
                'label'     => __('Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-headline__value, {{WRAPPER}} .typed-cursor' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'headline_typo',
                'selector' => '{{WRAPPER}} .ael-headline__value, {{WRAPPER}}  .typed-cursor',
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'     => 'headline_background',
                'selector' => '{{WRAPPER}} .ael-headline__value, {{WRAPPER}}  .typed-cursor',
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();

        include dirname(__FILE__) . '/fancy-headline-view.php';

    }
}
