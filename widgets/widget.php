<?php
namespace Astro_Element\Elementor;

class Elementor_Init
{

    public function __construct()
    {

        add_action('elementor/widgets/widgets_registered', array($this, 'on_widgets_registered'));
        add_action('elementor/init', array($this, 'register_category'));

    }

    public function on_widgets_registered()
    {
        $this->include_part();
        $this->register_widget();
    }

    public function include_part()
    {
        include dirname(__FILE__) . '/heading/heading.php';
        include dirname(__FILE__) . '/fancy-headline/fancy-headline.php';
        include dirname(__FILE__) . '/advanced-heading/advanced-heading.php';
        include dirname(__FILE__) . '/map/map.php';
        include dirname(__FILE__) . '/contact-form-7/contact-form-7.php';
        include dirname(__FILE__) . '/countdown/countdown.php';
        include dirname(__FILE__) . '/pricing/pricing-table.php';
        include dirname(__FILE__) . '/slider/slider.php';
        include dirname(__FILE__) . '/testimonial/testimonial.php';
        include dirname(__FILE__) . '/team-box/team-box.php';
        include dirname(__FILE__) . '/post/posts.php';
        include dirname(__FILE__) . '/banner/banner.php';
        include dirname(__FILE__) . '/content-box/content-box.php';
        include dirname(__FILE__) . '/cta/cta.php';
        include dirname(__FILE__) . '/image-slider/image-slider.php';

        if (class_exists( 'woocommerce' )){
            include dirname(__FILE__) . '/product-cat/product-cat.php';
            include dirname(__FILE__) . '/product/product.php';
        }
        
        include dirname(__FILE__) . '/template-carousel/template-carousel.php';
        include dirname(__FILE__) . '/portfolio/portfolio.php';
        include dirname(__FILE__) . '/menu/menu.php';


        // deprecated
        include dirname(__FILE__) . '/deprecated/testimonial-carousel/testimonial-carousel.php';
        include dirname(__FILE__) . '/deprecated/team/team.php';
        include dirname(__FILE__) . '/deprecated/team-carousel/team-carousel.php';

    }   

    /**
     * register Category
     * @since 1.0.0
     * @return [add new category]
     */
    public function register_category()
    {
        \Elementor\Plugin::instance()->elements_manager->add_category(
            'astro-element',
            [
                'title' => 'Astro Elements',
                'icon'  => 'font',
            ],
            2
        );

        \Elementor\Plugin::instance()->elements_manager->add_category(
            'astro-element-deprecated',
            [
                'title' => 'Deprecated',
                'icon'  => 'font',
            ],
            99
        );

    }

    /**
     * Register Widget
     *
     * @since 1.0.0
     *
     * @access private
     */
    private function register_widget()
    {   
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Posts());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Map());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Contact_Form_7());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Countdown());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Pricing_Table());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Slider());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Heading());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Fancy_Headline());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Advanced_Heading());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Testimonial());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Team_Box());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Banner());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Content_Box());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new CTA());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Image_Slider());
        
        if (class_exists( 'WooCommerce' )){
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Product_Cat());
        }

        if(function_exists('rt_setup') && class_exists( 'WooCommerce' )){
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Product());
        }
        
        if (ael_is_dev()) {
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Template_Carousel());
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Portfolio());
        }
        //deprecated
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Testimonial_Carousel());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Team());
        \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Team_Carousel());
    }

    // end class
}

new Elementor_Init;
