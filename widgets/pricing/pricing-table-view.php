
<?php
$website_link = $settings['footer_url'];
$url = $website_link['url'];
$target = $website_link['is_external'] ? 'target="_blank"' : '';

$currency = !empty($settings['currency_custom'])?$settings['currency_custom']:astro_get_currency($settings['currency']);
?>


<div id="price-table-<?php echo $this->get_id()?>" class="ael-price-table">


	<div class="ael-price-table__header">

		<div class="ael-price-table__title"><?php echo $settings['title'] ?></div>

		<?php if(!empty($settings['subtitle'])): ?>
		<div class="ael-price-table__sub-title"><?php echo $settings['subtitle'] ?></div>
		<?php endif ?>

		<?php if ($settings['ribbon'] === 'yes'): ?>
			<span class="ael-price-table__ribbon">
				<div class="ael-price-table__ribbon-inner"><?php echo $settings['ribbon_title'] ?></div>
			</span>
		<?php endif?>


	</div>

	<div class="ael-price-table__price-wrapper">

		<?php if(!empty($settings['sale'])): ?>
			<div class="ael-price-table__original-price"><?php echo $currency.' '.$settings['price'] ?></div>
		<?php endif ?>

		<span class="ael-price-table__currency"><?php echo $currency ?></span>
		<span class="ael-price-table__price"><?php echo $price_number ?></span>
		
		<?php if(!empty($after_price)): ?>
		<span class="ael-price-table__after-price"><?php echo $after_price ?></span>
		<?php endif ?>
		
		<?php if (!empty($settings['period'])): ?>
			<span class="ael-price-table__priod"><?php echo $settings['period'] ?></span>
		<?php endif?>

	</div>

	<ul class="ael-price-table__featured">
		<?php foreach ( $settings['featured'] as $index => $item ) :?>
		<?php $property_class = ($item['disable'] == 'yes')?'is-disable':'';?>

		 <?php if ($property_class): ?>
		 		<li class="<?php echo $property_class ?>">
			<?php else: ?>
				<li>
		 <?php endif; ?>


			<?php if (!empty($item['icon'])): ?>
				<i class="<?php echo $item['icon']?>"></i>
			<?php endif; ?>

			<?php echo $item['item'] ?>
		</li>
		<?php endforeach; ?>
	</ul>
	<div class="ael-price-table__footer">

		<?php if ( $settings['footer_text']): ?>
			<a href="<?php echo $url  ?>" <?php echo $target  ?> class="ael-price-table__btn rt-btn"><?php echo $settings['footer_text'] ?></a>
		<?php endif; ?>
		
		<?php if ($settings['footer_info']): ?>
			<p><?php echo $settings['footer_info'] ?></p>
		<?php endif?>
	</div>
	
</div>

