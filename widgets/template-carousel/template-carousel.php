<?php
namespace Astro_Element\Elementor;

use Astro_Element\Helper;
use Astro_Element\HTML;
use Elementor\Controls_Manager;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Template_Carousel extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-template-carousel';
    }

    public function get_title()
    {
        return __('Template Carousel', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post-slider';
    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    protected function _register_controls()
    {
        $this->start_controls_section(
            'setting_content',
            [
                'label' => __('Content', 'astro-element'),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'label', [
                'label'   => __('Label', 'astro-element'),
                'type'    => Controls_Manager::TEXT,
                'default' => 'Label',
            ]
        );

        $repeater->add_control(
            'content_type', [
                'label'   => __('Content Type', 'astro-element'),
                'type'    => Controls_Manager::SELECT,
                'options' => [
                    'editor'   => __('Editor', 'astro-element'),
                    'template' => __('Template', 'astro-element'),
                    'html'     => __('HTML', 'astro-element'),
                ],
                'default' => 'editor',
            ]
        );

        $repeater->add_control(
            'content', [
                'label'     => __('Content', 'astro-element'),
                'type'      => Controls_Manager::WYSIWYG,
                'default'   => 'Carousel Item Content',
                'condition' => [
                    'content_type' => 'editor',
                ],
            ]
        );

        $repeater->add_control(
            'html', [
                'label'     => __('HTML', 'astro-element'),
                'type'      => Controls_Manager::CODE,
                'condition' => [
                    'content_type' => 'html',
                ],

            ]
        );

        $repeater->add_control(
            'template', [
                'label'     => __('Choose Template', 'astro-element'),
                'type'      => Controls_Manager::SELECT2,
                'options'   => Helper::get_posts('elementor_library'),
                'condition' => [
                    'content_type' => 'template',
                ],

            ]
        );

        $this->add_control(
            'slider',
            [
                'label'       => __('Slider', 'astro-element'),
                'type'        => Controls_Manager::REPEATER,
                'fields'      => $repeater->get_controls(),
                'title_field' => '{{{ label }}}',
            ]
        );

        $this->end_controls_section();

        $this->setting_carousel([
            'carousel' => 'false',
        ]);
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        echo HTML::before_slider(array(
            'id'         => 'content-slider-' . $this->get_id(),
            'items-lg'   => $settings['slider_item'],
            'items-md'   => $settings['slider_item_tablet'],
            'items-sm'   => $settings['slider_item_mobile'],
            'nav'        => ($settings['slider_nav'] != 'none' && $settings['slider_nav'] != 'header') ? true : false,
            'gap'        => $settings['slider_gap'],
            'pagination' => ($settings['slider_pagination'] === 'yes') ? true : false,
            'loop'       => $settings['slider_loop'],
            'autoplay'   => $settings['slider_auto_play'],
        ));

        echo HTML::open('ael-slider__main owl-carousel');

        foreach ($settings['slider'] as $key => $content) {
            include dirname(__FILE__) . '/template-carousel-view.php';
        }
        echo HTML::close();

        echo HTML::after_slider();

    }
}
