<?php 
$link_target = $item['link']['is_external'] ? ' target="_blank"' : '';
$link_nofollow = $item['link']['nofollow'] ? ' rel="nofollow"' : '';
?>

<div class="flex-item">

    <div class="ael-content-box">

        <?php if(!empty($item['image']['id'])): ?>
        <?php if(!empty($item['link']['url'])): ?>
        <a href="<?php echo $item['link']['url']?>" <?php echo  $link_target . $link_nofollow?>>
        <?php endif ?>

        <div class="ael-content-box__thumbnail rt-img rt-img--full">
            <?php echo wp_get_attachment_image($item['image']['id'], $settings['image_size']); ?>
        </div>

         <?php if(!empty($item['link']['url'])): ?>
        </a>
        <?php endif ?>
        <?php endif ?>

        <div class="ael-content-box__body">
            
            <h3 class="ael-content-box__title">

                <?php if (!empty($item['link']['url'])): ?>
                <a href="<?php echo $item['link']['url']?>" <?php echo  $link_target . $link_nofollow?>>
                <?php endif?>
                
                <?php echo $item['title'] ?>

                <?php if (!empty($item['link']['url'])): ?>
                </a>
                <?php endif?>
            </h3>

            <?php if(!empty($item['content'] )): ?>
            <div class="ael-content-box__content"><?php echo $item['content'] ?></div>
            <?php endif ?>

        </div>
    </div>
    
</div>

