<div class=ael-content-slider__item">
    
    <?php 
        if($content['content_type'] == 'html'){
            echo $content['html'];
        }

        if($content['content_type']== 'editor'){
            echo $content['editor'];
        }

        if($content['content_type']== 'template'){
            echo Elementor\Plugin::instance()->frontend->get_builder_content_for_display($content['template']);
        }
    ?>

</div>