<?php
namespace Astro_Element\Elementor;

use Astro_Element\Helper;
use Astro_Element\HTML;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Portfolio extends  \Astro_Element\Elementor_Base
{
    protected $post_type = 'portfolio';
    protected $post_taxonomy = 'portfolio-category';

    public function get_name()
    {
        return 'astro-portfolio';
    }

    public function get_title()
    {
        return __('Portfolio', 'astro-element');
    }

    public function get_icon()
    {
         return 'ate-icon ate-portfolio';
    }

    	public function get_categories() {
		return ['astro-element'];
	}

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_options();
        $this->setting_pagination();
        
        $this->style_general();
        $this->style_body();
        $this->style_title();
        $this->style_meta();

        $this->setting_button(array(
            'name' => 'readmore',
            'class' => '.ael-post__readmore',
            'label' => 'Read More',
        ));

        $this->setting_button(array(
            'name' => 'loadmore',
            'label' => 'Load More',
            'class' => '.rt-pagination__button',
        ));


        $this->setting_carousel();

    }

    /**
     *  query post
     * @return [query section]
     */
    public function setting_query()
    {
        $this->start_controls_section(
            'setting_query',
            [
                'label' => __('Query', 'astro-element'),
            ]
        );

        $this->add_control(
            'post_type',
            [
                'label' => __('Post Type', 'astro-element'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => $this->post_type,
            ]
        );

       $this->add_control(
                'posts_per_page',
                [
                    'label' => __('Posts Number', 'astro-element'),
                    'type' => Controls_Manager::NUMBER,
                    'default' => 6,
                ]
            );

        $this->add_control(
            'advanced',
            [
                'label' => __('Advanced', 'astro-element'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );
        $this->add_control(
            'query_by',
            [
                'label' => __('Query posts by', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'lastest',
                'options' => [
                    'lastest' => __('Lastest Posts', 'astro-element'),
                    'category' => __('Category', 'astro-element'),
                    'manually' => __('Manually', 'astro-element'),
                ],
            ]
        );

        $this->add_control(
            'category',
            [
                'label' => __('Categories', 'astro-element'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_terms($this->post_taxonomy),
                'condition' => [
                    'query_by' => 'category',
                ],
            ]
        );
        $this->add_control(
            'post_id',
            [
                'label' => __('Select Post', 'astro-element'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_posts($this->post_type),
                'condition' => [
                    'query_by' => 'manually',
                ],

            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'ID' => 'Post Id',
                    'author' => 'Post Author',
                    'title' => 'Title',
                    'date' => 'Date',
                    'modified' => 'Last Modified Date',
                    'parent' => 'Parent Id',
                    'rand' => 'Random',
                    'comment_count' => 'Comment Count',
                    'menu_order' => __('Menu Order', 'astro-element'),
                    'wp_post_views_count' => __('Most Viewer', 'astro-element'),
                    'comment_count' => __('Most Review', 'astro-element'),
                ],
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'DESC',
                'options' => [
                    'ASC' => __('ASC', 'astro-element'),
                    'DESC' => __('DESC', 'astro-element'),
                ],
            ]
        );

        $this->add_control(
            'offset',
            [
                'label' => __('Offset', 'astro-element'),
                'type' => Controls_Manager::NUMBER,
                'default' => 0,
                'description' => __('Use this setting to skip over posts (e.g. \'2\' to skip over 2 posts).', 'astro-element'),
            ]
        );

        $this->end_controls_section();
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-element'),
            ]
        );

         $this->add_control(
            'portfolio_style',
            [
                'label' => __('Style', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'grid',
                'options' => [
                    'grid' => 'Grid',
                    'overlay'=> 'Overlay',
                    'transform' => 'Transform',
                ],
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => __(1, 'rt_domain'),
                    2 => __(2, 'rt_domain'),
                    3 => __(3, 'rt_domain'),
                    4 => __(4, 'rt_domain'),
                    6 => __(6, 'rt_domain'),
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );
        $this->add_control(
            'layout_masonry',
            [
                'label' => __('Masonry', 'astro-element'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', 'astro-element'),
                'label_on' => __('On', 'astro-element'),
                'return_value' => 'yes',
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'meta_category',
            [
                'label' => __('Category', 'astro-element'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_off' => __('Off', 'astro-element'),
                'label_on' => __('On', 'astro-element'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Featured Image', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );


        $this->end_controls_section();
    }

 

    protected function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                  'selectors' => [
                    '{{WRAPPER}} .ael-loop' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .ael-loop > .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                    padding-right: calc({{SIZE}}{{UNIT}}/2);
                                                    margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_responsive_control(
            'general_alignment',
            [
                'label' => __('Layout Alignment', 'astro-element'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'astro-element'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro-element'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'astro-element'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .ael-portfolio__meta,
                    {{WRAPPER}} .ael-portfolio__body' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        // layout tab hover control
        $this->add_control(
            'general_background',
            [
                'label' => __('Background Options', 'astro-element'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->start_controls_tabs('general_background_tabs');

        $this->start_controls_tab(
            'general_background_normal',
            [
                'label' => __('Normal', 'astro-element'),
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'general_shadow',
                'selector' => '{{WRAPPER}} .ael-portfolio',

            ]
        );

        $this->end_controls_tab();
        $this->start_controls_tab(
            'general_background_tab_hover',
            [
                'label' => __('Hover', 'astro-element'),
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'general_shadow_hover',
                'selector' => '{{WRAPPER}} .ael-portfolio:hover',

            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'general_border',
                'selector' => '{{WRAPPER}} .ael-portfolio',
            ]
        );

        $this->add_responsive_control(
            'general_radius',
            [
                'label' => __('Border Radius', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 80,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .ael-portfolio' => 'border-radius: {{SIZE}}{{UNIT}};
                                                    overflow: hidden;',
                ],
            ]
        );

        $this->end_controls_section();
    }

    public function style_body()
    {

        $this->start_controls_section(
            'style_body',
            [
                'label' => __('Body', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'body_background',
            [
                'label' => __('Background Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-portfolio' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'body_padding',
            [
                'label' => __('Spacing', 'astro-element'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .ael-portfolio__body' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();

    }

    public function style_title()
    {
        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('title_tabs');
        $this->start_controls_tab(
            'title_normal',
            [
                'label' => __('Normal', 'astro-element'),
            ]
        );
        $this->add_control(
            'title_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-portfolio__title' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'title_hover',
            [
                'label' => __('Hover', 'astro-element'),
            ]
        );
        $this->add_control(
            'title_color_hover',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-portfolio__title:hover' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'selector' => '{{WRAPPER}} .ael-portfolio__title',
            ]
        );

        $this->end_controls_section();
    }

    public function style_meta()
    {
        $this->start_controls_section(
            'style_meta',
            [
                'label' => __('Category', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'meta_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-portfolio__category a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'meta_typography',
                'selector' => '{{WRAPPER}} .ael-portfolio__category ',
            ]
        );

        $this->end_controls_section();
    }



    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $args = array(
            'id' => $this->get_id(),
            'template_part' => 'core/elementor/portfolio/portfolio-view',
            'class' => 'ael-portfolio',
        );

        echo $this->elementor_loop(wp_parse_args($args, $settings));
        

        
    }
    /* end class */
}
