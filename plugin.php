<?php

if (!defined('ABSPATH')) {
    exit;
}
// Exit if accessed directly

/**
 * Main Plugin Class
 *
 * Register new elementor widget.
 *
 * @since 1.0.0
 */
class Astro_Element
{
   

    /**
     * Constructor
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function __construct()
    {    

        // Check if Elementor installed and activated
        add_action('admin_notices', array($this, 'admin_notice_missing_main_plugin'));

        add_action('wp_enqueue_scripts', array($this, 'register_style'), 99);
        add_action('elementor/frontend/after_register_scripts', array($this, 'register_script'));
        add_action('elementor/editor/after_enqueue_scripts', array($this, 'elementor_scripts'));

        load_plugin_textdomain('astro-element', false, dirname(plugin_basename(__FILE__)) . '/languages/');


    }


    public function register_style()
    {
        wp_enqueue_style('owl-carousel', ASTRO_ELEMENT_ASSETS.'assets/css/owl.carousel.min.css', '', '4.2.2', 'all');
        wp_enqueue_style('font-awesome', ASTRO_ELEMENT_ASSETS.'assets/css/fontawesome.min.css', '', '4.7.0', 'all');
        
        if(!function_exists('rt_setup')){
            wp_enqueue_style('retheme-base', ASTRO_ELEMENT_ASSETS . 'assets/css/retheme-base.min.css', '', '1.0.0', 'all');
        }
        
        wp_enqueue_style('astro-element', ASTRO_ELEMENT_ASSETS.'assets/css/astro-element.min.css', '', '1.3.0', 'all');

    }

    public function elementor_scripts()
    {
        wp_enqueue_style('astro-elementor-editor', ASTRO_ELEMENT_ASSETS.'assets/css/astro-elementor-editor.css');

    }

    public function register_script()
    {

        wp_enqueue_script('jquery');
        wp_enqueue_script('masonry', ASTRO_ELEMENT_ASSETS.'assets/js/masonry.pkgd.min.js', array('jquery'), '4.2.2', true);
        wp_enqueue_script('typed', ASTRO_ELEMENT_ASSETS.'assets/js/typed.min.js', array('jquery'), '2.3.4', true);
        wp_enqueue_script('jquery.countdown', ASTRO_ELEMENT_ASSETS.'assets/js/jquery.countdown.min.js', array('jquery'), '2.3.4', true);
        wp_enqueue_script('owl-carousel', ASTRO_ELEMENT_ASSETS.'assets/js/owl.carousel.min.js', array('jquery'), '2.3.4', true);
        wp_enqueue_script('velocity', ASTRO_ELEMENT_ASSETS.'assets/js/velocity.min.js', false, '1.5.0', true);
        wp_enqueue_script('velocity-ui', ASTRO_ELEMENT_ASSETS.'assets/js/velocity.ui.min.js', array('velocity'), '5.2.0', true);
        wp_enqueue_script('astro-element', ASTRO_ELEMENT_ASSETS.'assets/js/astro-element.min.js', array('jquery'), '1.1.0', true);
    }

    /**
     * ADMIN NOTICE CHECK ELEMENTOR
     * This function check plugin elementor active
     * if elementor not active this show notice link for elementor instalation
     *
     * @return void
     */
    public function admin_notice_missing_main_plugin()
    {
        if (!did_action('elementor/loaded')) {
            /** link install elementor */
            $plugin_install = wp_nonce_url(
                add_query_arg(
                    array(
                        'action' => 'install-plugin',
                        'plugin' => 'elementor',
                    ),
                    admin_url('update.php')
                ),
                'install-plugin' . '_' . 'elementor'
            );

            if (isset($_GET['activate'])) {
                unset($_GET['activate']);
            }

            /** Show notice elementor required plugin */
            $message = sprintf(
                esc_html__('"%1$s" requires "%2$s" to be installed and activated. %3$s', 'astro-element'),
                '<strong>' . esc_html__(ASTRO_ELEMENT_VERSION, 'astro-element') . '</strong>',
                '<strong>' . esc_html__('Elementor', 'astro-element') . '</strong>',
                '<br><a class="button-primary" href=' . $plugin_install . ' style="margin-top: 10px;">Install Elementor</a class="button-primary">'
            );

            printf('<div class="notice notice-error is-dismissible"><p>%1$s</p></div>', $message);
        }

    }

}

new Astro_Element();
