<?php
namespace Astro_Element;

use Astro_Element\HTML;

class Helper
{

    /**
     * Get animation list
     * @return [array]
     */
    public static function get_animation_in()
    {
        return array(
            'transition.fadeIn' => __('fade', 'admin_domain'),
            'callout.bounce' => __('Bounce', 'admin_domain'),
            'callout.shake' => __('Shake', 'admin_domain'),
            'callout.flash' => __('Flash', 'admin_domain'),
            'callout.pulse' => __('Pulse', 'admin_domain'),
            'callout.swing' => __('Fade', 'admin_domain'),
            'callout.tada' => __('Swing', 'admin_domain'),
            'transition.flipXIn' => __('flipX', 'admin_domain'),
            'transition.flipYIn' => __('flipY', 'admin_domain'),
            'transition.flipBounceXIn' => __('flipBounceX', 'admin_domain'),
            'transition.flipBounceYIn' => __('flipBounceY', 'admin_domain'),
            'transition.swoopIn' => __('swoop', 'admin_domain'),
            'transition.whirlIn' => __('whirl', 'admin_domain'),
            'transition.shrinkIn' => __('shrink', 'admin_domain'),
            'transition.expandIn' => __('expand', 'admin_domain'),
            'transition.bounceUpIn' => __('bounceUp', 'admin_domain'),
            'transition.bounceDownIn' => __('bounceDown', 'admin_domain'),
            'transition.bounceLeftIn' => __('bounceLeft', 'admin_domain'),
            'transition.bounceRightIn' => __('bounceRight', 'admin_domain'),
            'transition.slideUpIn' => __('slideUp', 'admin_domain'),
            'transition.slideDownIn' => __('slideDown', 'admin_domain'),
            'transition.slideLeftIn' => __('slideLeft', 'admin_domain'),
            'transition.slideRightIn' => __('slideRight', 'admin_domain'),
            'transition.slideUpBigIn' => __('slideUpBig', 'admin_domain'),
            'transition.slideDownBigIn' => __('slideDownBig', 'admin_domain'),
            'transition.slideLeftBigIn' => __('slideLeftBig', 'admin_domain'),
            'transition.slideRightBigIn' => __('slideRightBig', 'admin_domain'),
            'transition.perspectiveUpIn' => __('perspectiveUp', 'admin_domain'),
            'transition.perspectiveDownIn' => __('perspectiveDown', 'admin_domain'),
            'transition.perspectiveLeftIn' => __('perspectiveLeft', 'admin_domain'),
            'transition.perspectiveRightIn' => __('perspectiveRight', 'admin_domain'),
        );
    }

    /**
     * Get animation out list
     * @return [array]
     */
    public static function get_animation_out()
    {
        return array(
            'transition.fadeOut' => __('fadeOut', RT_THEME_DOMAIN),
            'callout.bounce' => __('Bounce', 'admin_domain'),
            'callout.shake' => __('Shake', 'admin_domain'),
            'callout.flash' => __('Flash', 'admin_domain'),
            'callout.pulse' => __('Pulse', 'admin_domain'),
            'callout.swing' => __('Fade', 'admin_domain'),
            'callout.tada' => __('Swing', 'admin_domain'),
            'transition.flipXOut' => __('flipXOut', RT_THEME_DOMAIN),
            'transition.flipYOut' => __('flipYOut', RT_THEME_DOMAIN),
            'transition.flipBounceXOut' => __('flipBounceXOut', RT_THEME_DOMAIN),
            'transition.flipBounceYOut' => __('flipBounceYOut', RT_THEME_DOMAIN),
            'transition.swoopOut' => __('swoopOut', RT_THEME_DOMAIN),
            'transition.whirlOut' => __('whirlOut', RT_THEME_DOMAIN),
            'transition.shrinkOut' => __('shrOutkOut', RT_THEME_DOMAIN),
            'transition.expandOut' => __('expandOut', RT_THEME_DOMAIN),
            'transition.bounceUpOut' => __('bounceUpOut', RT_THEME_DOMAIN),
            'transition.bounceDownOut' => __('bounceDownOut', RT_THEME_DOMAIN),
            'transition.bounceLeftOut' => __('bounceLeftOut', RT_THEME_DOMAIN),
            'transition.bounceRightOut' => __('bounceRightOut', RT_THEME_DOMAIN),
            'transition.slideUpOut' => __('slideUpOut', RT_THEME_DOMAIN),
            'transition.slideDownOut' => __('slideDownOut', RT_THEME_DOMAIN),
            'transition.slideLeftOut' => __('slideLeftOut', RT_THEME_DOMAIN),
            'transition.slideRightOut' => __('slideRightOut', RT_THEME_DOMAIN),
            'transition.slideUpBigOut' => __('slideUpBigOut', RT_THEME_DOMAIN),
            'transition.slideDownBigOut' => __('slideDownBigOut', RT_THEME_DOMAIN),
            'transition.slideLeftBigOut' => __('slideLeftBigOut', RT_THEME_DOMAIN),
            'transition.slideRightBigOut' => __('slideRightBigOut', RT_THEME_DOMAIN),
            'transition.perspectiveUpOut' => __('perspectiveUpOut', RT_THEME_DOMAIN),
            'transition.perspectiveDownOut' => __('perspectiveDownOut', RT_THEME_DOMAIN),
            'transition.perspectiveLeftOut' => __('perspectiveLeftOut', RT_THEME_DOMAIN),
            'transition.perspectiveRightOut' => __('perspectiveRightOut', RT_THEME_DOMAIN),
        );
    }

    /**
     * Get social media list
     * @return [array]
     */
    public static function get_social_media()
    {
        return array(
            'facebook' => 'Facebook',
            'twitter' => 'Twitter',
            'instagram' => 'Instagram',
            'youtube' => 'Youtube',
            'rss' => 'RSS',
            'vimeo' => 'Vimeo',
            'pinterest' => 'Pinterest',
            'dribbble' => 'Dribbble',
            'whatsapp' => 'Whatsapp',
            'telegram-plane ' => 'Telegram',
            'wordpress' => 'Wordpress',
            'behance' => 'Behance',
            'dropbox' => 'Dropbox',
            'soundcloud' => 'Soundcloud',
            'tumblr' => 'Tumblr',
            'github' => 'Github',
            'gitlab' => 'Gitlab ',
            'medium' => 'Medium',
            'dribbble ' => 'Dribbble',
            'slack-hash' => 'Slack',
        );
    }

    /**
     * Get terms wp taxonomy
     * @param  [taxonomy name] $term [insert taxonomy name]
     * @return [array]
     */
    public static function get_terms($term, $id = '')
    {
        $terms = array();

        if (!empty($id)) {
            $terms = wp_get_post_terms($id, $term);
        } else {
            $terms = get_terms($term);
        }

        if (!empty($terms) && !is_wp_error($terms)) {
            foreach ($terms as $key => $term) {
                $data[$term->term_id] = $term->name;
            }
        } else {
            $data = array('Not found terms');
        }

        return $data;
    }

    /**
     * Get all post list
     * @param  [post type] $post_type [insert post type]
     * @param  string $perpage   [count post list]
     * @return [array]
     */
    public static function get_posts($post_type, $perpage = '')
    {
        $post_type_name = str_replace('-', ' ', $post_type);

        $args = array(
            'post_type' => $post_type,
            'posts_per_page' => $perpage,
        );

        $posts = get_posts($args);

        if ($posts) {
            foreach ($posts as $post) {
                setup_postdata($post);

                $data[get_the_id()] = get_the_title();
            }
            wp_reset_postdata();
        } else {
            $data[] = sprintf(__('No %s result', RT_THEME_DOMAIN), $post_type_name);
        }

        return $data;
    }

    /**
     * Get all user
     * @return [array]
     */
    public static function get_user()
    {
        $args = array(
            'orderby' => 'display_name',
        );

        // Create the WP_User_Query object
        $users = get_users();
        foreach ($users as $key => $user) {
            $data[$user->ID] = $user->display_name;
        }

        return $data;
    }

    public static function include_part($template = '')
    {
        include dirname(__FILE__) . '/' . $template . '.php';
    }

    /**
     * Set css class attribute with filter hook
     * @param string $filter  [filter hoook]
     * @param array  $classes [classes]
     */
    public static function set_class($filter = '', $classes = array())
    {
        if (!empty($filter)) {
            $class_output = apply_filters($filter, join(' ', array_unique($classes)));
        } else {
            $class_output = join(' ', array_unique($classes));
        }
        return 'class="' . $class_output . '"';
    }

    /**
     * Get an array of all available post type.
     * @return [array]
     */
    public static function get_post_types($post_type = '')
    {
        $items = array();

        // Get the post types.
        $post_types = get_post_types(
            array(
                'public' => true,
            ),
            'objects'
        );

        // add all choose
        if ($post_type) {
            $item[''] = $post_type;
        }

        // Build the array.
        foreach ($post_types as $post_type) {
            $items[$post_type->name] = $post_type->labels->name;
        }
        return $items;
    }

    /**
     * Get an array of publicly-querable taxonomies.
     *
     * @static
     * @access public
     * @return array
     */
    public static function get_taxonomies()
    {
        $items = array();

        // Get the taxonomies.
        $taxonomies = get_taxonomies(
            array(
                'public' => true,
            )
        );

        // Build the array.
        foreach ($taxonomies as $taxonomy) {
            $id = $taxonomy;
            $taxonomy = get_taxonomy($taxonomy);
            $items[$id] = $taxonomy->labels->name;
        }

        return $items;
    }

    /**
     * Get image list
     * @return [array]
     */

    public static function get_image_size()
    {
        $data['none'] = 'Hidden Image';
        $data['full'] = 'Original Image';

        foreach (get_intermediate_image_sizes() as $key => $image) {
            $data[$image] = $image;
        }

        return $data;
    }

    /**
     * reuse custom query argument
     * @param array $args ['query argument']
     * @return [array]
     */
    public static function query($args = array())
    {

        $query_by = array();

        // post type
        if (!empty($args['post_type'])) {
            $query_by['post_type'] = $args['post_type'];
        }

        // post per page
        if (!empty($args['posts_per_page'])) {
            $query_by['posts_per_page'] = $args['posts_per_page'];
        }

        // paged
        if (!empty($args['paged'])) {
            $query_by['paged'] = $args['paged'];
        }

        // query by category
        if (!empty($args['query_by']) && $query_by['post_type'] == 'post' && $args['query_by'] == 'category') {
            $query_by['cat'] = $args['category'];
        }

        // query by tags
        if (!empty($args['query_by']) && $args['query_by'] == 'tags') {
            $query_by['tag__and'] = $args['tags'];
        }

        // query by manually
        if (!empty($args['query_by']) && $args['query_by'] == 'manually') {
            $query_by['post__in'] = $args['post_id'];
        }

        // query by woocommerce featured product
        if (!empty($args['query_by']) && $query_by['post_type'] == 'product' && $args['query_by'] == 'featured') {
            $query_by['tax_query'][] = array(
                'taxonomy' => 'product_visibility',
                'field' => 'name',
                'terms' => 'featured',
                'operator' => 'IN',
            );
        }

        // query by woocommerce category
        if (!empty($args['query_by']) && $query_by['post_type'] == 'product' && $args['query_by'] == 'category') {
            $query_by['tax_query'][] = array(
                'taxonomy' => 'product_cat',
                'field' => 'term_id',
                'terms' => $args['category'],
                'operator' => 'IN',

            );
        }

        // query by term
        if (!empty($args['query_by']) && $args['query_by'] == 'term') {
            $query_by['tax_query'][] = array(
                'taxonomy' => $args['taxonomy'],
                'field' => 'term_id',
                'terms' => $args['term'],
                'operator' => 'IN',

            );
        }

        // featured post
        if (!empty($args['query_by']) && $args['query_by'] == 'post_featured') {
            $query_by['meta_query'][] = array(
                'key' => 'post_featured',
                'value' => 1,
            );
        }

        // order by general
        if (!empty($args['orderby'])) {
            $query_by['orderby'] = $args['orderby'];
        }

        // order by total sale
        if (!empty($args['orderby']) && $args['orderby'] == 'total_sales') {
            $query_by['meta_key'] = 'total_sales';
            $query_by['orderby'] = 'meta_value_num';
        }

        // order by Most Viewer
        if (!empty($args['orderby']) && $args['orderby'] == 'wp_post_views_count') {
            $query_by['meta_key'] = 'wp_post_views_count';
            $query_by['orderby'] = 'meta_value_num';
        }

        // order by most comment
        if (!empty($args['orderby']) && $args['orderby'] == 'comment_count') {
            $query_by['orderby'] = 'comment_count';
        }

        // order
        if (!empty($args['order'])) {
            $query_by['order'] = $args['order'];
        }

        // Offset
        if (!empty($args['offset'])) {
            $query_by['offset'] = $args['offset'];
        }

        // Default Argument
        $query_default = array(
            'post_type' => 'post',
            'posts_per_page' => 5,
            'post_status' => 'publish',
        );
        $query_args = wp_parse_args($query_by, $query_default);
        $widget_id = !empty($args['id']) ? $args['id'] : 1;

        // Merge Array
        return apply_filters("astro_element_query_widget_{$widget_id}", $query_args);
    }

    // end class
}
