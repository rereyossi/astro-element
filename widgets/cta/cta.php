<?php
namespace Astro_Element\Elementor;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class CTA extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-cta';
    }

    public function get_title()
    {
        return __('CTA', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post-slider';

    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    protected function _register_controls()
    {

        $this->setting_content();

        $this->style_general();
        $this->style_title();
        $this->style_overtext();
        $this->style_subtitle();
        $this->style_content();

        $this->setting_button(array(
            'name'  => 'button-1',
            'label' => 'Button 1',
            'class' => '.rt-btn--primary',
        ));
        $this->setting_button(array(
            'name'  => 'button-2',
            'label' => 'Button 2',
            'class' => '.rt-btn--second',
        ));

    }

    public function setting_content()
    {
        $this->start_controls_section(
            'setting_content',
            [
                'label' => __('Content', 'astro-element'),
            ]
        );

         $this->add_control(
            'overtext', [
                'label'   => __('Over Text', 'astro-element'),
                'type'    => Controls_Manager::TEXT,
                'default' => __('This over text​', 'astro-element'),
            ]
        );

        $this->add_control(
            'title', [
                'label'   => __('Title', 'astro-element'),
                'type'    => Controls_Manager::TEXT,
                'default' => __('This is the heading​', 'astro-element'),
            ]
        );

        $this->add_control(
            'subtitle', [
                'label'   => __('Sub Title', 'astro-element'),
                'type'    => Controls_Manager::TEXT,
                'default' => __('This is the sub heading​', 'astro-element'),
            ]
        );

        $this->add_control(
            'content', [
                'label'   => __('Content', 'astro-element'),
                'type'    => Controls_Manager::TEXTAREA,
                'default' => __('Click edit button to change this text. Lorem ipsum dolor sit amet consectetur adipiscing elit dolor​', 'astro-element'),
            ]
        );

        $this->add_control(
            'button_primary', [
                'label'   => __('Primary Button Text', 'astro-element'),
                'type'    => Controls_Manager::TEXT,
                'default' => __('Button 1', 'astro-element'),
            ]
        );

        $this->add_control(
            'button_primary_link',
            [
                'label'         => __('Button Primary Link', 'astro-element'),
                'type'          => Controls_Manager::URL,
                'placeholder'   => __('https://your-link.com', 'astro-element'),
                'show_external' => true,
                'label_block'   => false,
                'default'       => [
                    'url'         => '',
                    'is_external' => false,
                    'nofollow'    => true,
                ],
            ]
        );

        $this->add_control(
            'button_second', [
                'label'   => __('Secondary Button Text', 'astro-element'),
                'type'    => Controls_Manager::TEXT,
                'default' => __('Button 2', 'astro-element'),
            ]
        );

        $this->add_control(
            'button_second_link',
            [
                'label'         => __('Button Second Link', 'astro-element'),
                'type'          => Controls_Manager::URL,
                'placeholder'   => __('https://your-link.com', 'astro-element'),
                'show_external' => true,
                'label_block'   => false,
                'default'       => [
                    'url'         => '',
                    'is_external' => false,
                    'nofollow'    => true,
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'     => 'general_background',
                'selector' => '{{WRAPPER}} .ael-cta',
            ]
        );

        $this->add_responsive_control(
            'general_height',
            [
                'label'      => __('Height', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'default'    => [
                    'size' => 270,
                ],
                'range'      => [
                    'px' => [
                        'min'  => 100,
                        'max'  => 700,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px'],

                'selectors'  => [
                    '{{WRAPPER}} .ael-cta' => 'min-height: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'general_width',
            [
                'label'      => __('Width', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'range'      => [
                    'px' => [
                        'min'  => 100,
                        'max'  => 700,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],

                'selectors'  => [
                    '{{WRAPPER}} .ael-cta__inner' => 'width: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
        $this->add_responsive_control(
            'general_alignment',
            [
                'label'     => __('Layout Alignment', 'astro-element'),
                'type'      => Controls_Manager::CHOOSE,
                'options'   => [
                    'left'   => [
                        'title' => __('Left', 'astro-element'),
                        'icon'  => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro-element'),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'right'  => [
                        'title' => __('Right', 'astro-element'),
                        'icon'  => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .ael-cta' => 'justify-content: {{VALUE}};',
                    '{{WRAPPER}} .ael-cta__inner' => 'text-align: {{VALUE}};'
                ],
            ]
        );

        $this->add_responsive_control(
            'general_padding',
            [
                'label'      => __('Padding', 'astro-element'),
                'type'       => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%', 'em'],
                'selectors'  => [
                    '{{WRAPPER}} .ael-cta' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();
    }

    
    public function style_title()
    {
        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'title_typography',
                'selector' => '{{WRAPPER}} .ael-cta__title',
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label'     => __('Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-cta__title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'title_spacing',
            [
                'label'      => __('Title Spacing', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'range'      => [
                    'px' => [
                        'min'  => 1,
                        'max'  => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .ael-cta__title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_overtext()
    {
        $this->start_controls_section(
            'style_overtext',
            [
                'label' => __('Over Text', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'overtext_typography',
                'selector' => '{{WRAPPER}} .ael-cta__overtext',
            ]
        );

        $this->add_control(
            'overtext_color',
            [
                'label'     => __('Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-cta__overtext' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'overtext_spacing',
            [
                'label'      => __('Over Text Spacing', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'range'      => [
                    'px' => [
                        'min'  => 1,
                        'max'  => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .ael-cta__overtext' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_subtitle()
    {
        $this->start_controls_section(
            'style_subtitle',
            [
                'label' => __('Sub Title', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'subtitle_typography',
                'selector' => '{{WRAPPER}} .ael-cta__subtitle',
            ]
        );

        $this->add_control(
            'subtitle_color',
            [
                'label'     => __('Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-cta__subtitle' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'subtitle_spacing',
            [
                'label'      => __('Sub Title Spacing', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'range'      => [
                    'px' => [
                        'min'  => 1,
                        'max'  => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .ael-cta__subtitle' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_content()
    {
        $this->start_controls_section(
            'style_content',
            [
                'label' => __('Content', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'content_typography',
                'selector' => '{{WRAPPER}} .ael-cta__content',
            ]
        );

        $this->add_control(
            'content_color',
            [
                'label'     => __('Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-cta__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'content_spacing',
            [
                'label'      => __('Content Spacing', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'range'      => [
                    'px' => [
                        'min'  => 1,
                        'max'  => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .ael-cta__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        ael_get_template_part('cta/cta-view', $settings);

    }
}
