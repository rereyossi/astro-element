<div class="ael-testimonial">
    <div class="ael-testimonial__content"><?php echo $testimonial['content'] ?></div> 
      
    <div class="ael-testimonial__thumbnail rt-img rt-img--full">
        <?php if($testimonial['image']['id']): ?>
             <?php echo wp_get_attachment_image($testimonial['image']['id'], 'thumbnail' ); ?>
        <?php else: ?>
            <img src="<?php echo $testimonial['image']['url']?>">
        <?php endif ?>
    </div>
    

    <div class="ael-testimonial__profile">
        <h4 class="ael-testimonial__name"><?php echo $testimonial['name'] ?></h4>
        <span class="ael-testimonial__position"><?php echo $testimonial['position'] ?></span>
    </div>

</div>