
== Changelog ==
= 2.7.0 (Released on 1 Juli 2021) =
* Fix: opsi pricing tablet elementor

v 1.6.0
- change prefix base from rt to ael

v 1.5.0
- fix ajax loop load more
- added wp nonce for security
- support elementor > 2.0

v 1.3.1
- Add new widgets product category