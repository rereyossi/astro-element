<?php 
$post_classes = !empty($settings['class'])?$settings['class']:"";
$date_url = esc_url(get_day_link(get_the_time('Y'), get_the_time('m'), get_the_time('d')));
$author_url = get_author_posts_url(get_the_author_meta('ID'));
?>

<div class="flex-item">

<div id="<?php echo 'post-'.get_the_ID()?>" <?php post_class($post_classes)?>>


<?php if (has_post_thumbnail() && $settings['image_size'] != 'none'): ?>
	<div class="ael-post__thumbnail rt-img rt-img--full">
		<?php the_post_thumbnail($settings['image_size'], 'img-responsive')?>
	</div>
<?php endif;?>

<div class="ael-post__body">

	<?php if (get_the_category() && $settings['meta_category']):?>
		<div class="ael-post__badges">
			<?php foreach (get_the_category() as $term):?>
			<a href="<?php echo get_category_link($term->term_id)?>" class="<?php echo $term->slug?>" ><?php echo $term->name?></a>
			<?php endforeach?>
		</div>
	<?php endif?>

	<h3 class="ael-post__title"><a href="<?php the_permalink( )?>"><?php the_title()?></a></h3>
	
	<div class="ael-post__meta">
		<?php if ($settings['meta_date']): ?>
		<a class="ael-post__meta-item date" href="<?php echo $date_url ?>"><i class="fa fa-calendar"></i><?php echo get_the_date() ?></a>
		<?php endif; ?>

		<?php if ($settings['meta_author']): ?>
		<a class="ael-post__meta-item author" href="<?php echo $author_url ?>"><i class="fa fa-user"></i><?php the_author()?></a>
		<?php endif; ?>

		<?php if ($settings['meta_comment'] && get_comments_number() >= 1): ?>
		<span class="ael-post__meta-item comment"><i class="fa fa-comment"></i><?php echo get_comments_number() ?></span>
		<?php endif?>

	</div>
	
	<?php if($settings['excerpt']):?>
		<div class="ael-post__content">
			<?php echo astro_the_content($settings['excerpt'])?>
		</div>
	<?php endif ?>

	<?php if ($settings['readmore']):?>
	<a href="<?php echo get_permalink()?>" class="ael-post__readmore"><?php echo $settings['readmore']?> <i class="fas fa-long-arrow-alt-right"></i></a>
	<?php endif?>

</div>
</div>
</div>