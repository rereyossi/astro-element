<?php

namespace Astro_Element\Elementor;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Advanced_Heading extends \Astro_Element\Elementor_Base
{

    public function get_name()
    {
        return 'astro-header-group';
    }

    public function get_title()
    {
        return __('Advanced Heading', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-headline';

    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    public function style_general()
    {

        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_align',
            [
                'label'     => __('Text Alignment', 'astro-element'),
                'type'      => Controls_Manager::CHOOSE,
                'options'   => [
                    'left'   => [
                        'title' => __('Left', 'astro-element'),
                        'icon'  => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro-element'),
                        'icon'  => 'fa fa-align-center',
                    ],
                    'right'  => [
                        'title' => __('Right', 'astro-element'),
                        'icon'  => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .ael-advanced-heading' => 'text-align: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();
    }

    public function style_title()
    {

        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_control(
            'title_color',
            [
                'label'     => __('Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-advanced-heading__title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'title_typo',
                'selector' => '{{WRAPPER}} .ael-advanced-heading__title',
            ]
        );

        $this->add_responsive_control(
            'title_spacing',
            [
                'label'      => __('Spacing', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'range'      => [
                    'px' => [
                        'min'  => 1,
                        'max'  => 50,
                        'step' => 1,
                    ],
                    '%'  => [
                        'min'  => 10,
                        'max'  => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .ael-advanced-heading__title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'     => 'title_background',
                'label'    => __('Background', 'astro-element'),
                'types'    => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .ael-advanced-heading__title',
            ]
        );

        $this->end_controls_section();
    }

    public function style_subtitle()
    {

        $this->start_controls_section(
            'style_subtitle',
            [
                'label' => __('Subtitle', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_control(
            'subtitle_color',
            [
                'label'     => __('Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-advanced-heading__subtitle' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'subtitle_typo',
                'selector' => '{{WRAPPER}} .ael-advanced-heading__subtitle',
            ]
        );

        $this->add_responsive_control(
            'subtitle_spacing',
            [
                'label'      => __('Spacing', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'range'      => [
                    'px' => [
                        'min'  => 1,
                        'max'  => 50,
                        'step' => 1,
                    ],
                    '%'  => [
                        'min'  => 10,
                        'max'  => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .ael-advanced-heading__subtitle' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_desc()
    {

        $this->start_controls_section(
            'style_desc',
            [
                'label' => __('Description', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_control(
            'desc_color',
            [
                'label'     => __('Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-advanced-heading__desc' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'desc_typo',
                'selector' => '{{WRAPPER}} .ael-advanced-heading__desc',
            ]
        );
        $this->add_responsive_control(
            'desc_spacing',
            [
                'label'      => __('Spacing', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'range'      => [
                    'px' => [
                        'min'  => 1,
                        'max'  => 50,
                        'step' => 1,
                    ],
                    '%'  => [
                        'min'  => 10,
                        'max'  => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors'  => [
                    '{{WRAPPER}} .ael-advanced-heading__desc' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );
        $this->end_controls_section();
    }

    public function style_shadow()
    {

        $this->start_controls_section(
            'style_shadow',
            [
                'label' => __('Shadow', 'astro-element'),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_control(
            'shadow_color',
            [
                'label'     => __('Color', 'astro-element'),
                'type'      => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-advanced-heading__shadow' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'     => 'shadow_typo',
                'selector' => '{{WRAPPER}} .ael-advanced-heading__shadow',
            ]
        );

        $this->add_responsive_control(
            'shadow_position',
            [
                'label'      => __('Position', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                                'range' => [
                    'px' => [
                        'min' => -100,
                        'max' => 100,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                        'step' => 1,
                    ],
                ],
                'selectors'  => [
                    '{{WRAPPER}} .ael-advanced-heading__shadow' => 'top: {{SIZE}}{{UNIT}};',
                ],

            ]
        );
        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name'     => 'shadow_background',
                'label'    => __('Background', 'astro-element'),
                'types'    => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .ael-advanced-heading__shadow',
            ]
        );
        $this->end_controls_section();
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_content',
            [
                'label' => __('Heading', 'astro-element'),
            ]
        );

        $this->add_control(
            'title',
            [
                'label'   => __('Title', 'astro-element'),
                'type'    => Controls_Manager::TEXT,
                'default' => 'Advanced Heading',
            ]
        );

        $this->add_control(
            'subtitle',
            [
                'label'   => __('Subtitle', 'astro-element'),
                'type'    => Controls_Manager::TEXT,
                'default' => 'Sub Heading',
            ]
        );

        $this->add_control(
            'desc',
            [
                'label'   => __('Description', 'astro-element'),
                'type'    => Controls_Manager::TEXTAREA,
                'default' => 'Description',
            ]
        );

        $this->add_control(
            'shadow',
            [
                'label'   => __('Shadow', 'astro-element'),
                'type'    => Controls_Manager::TEXT,
                'default' => 'Advanced Heading',
            ]
        );

        $this->end_controls_section();
    }

    protected function _register_controls()
    {
        $this->setting_options();

        $this->style_general();
        $this->style_title();
        $this->style_subtitle();
        $this->style_desc();
        $this->style_shadow();
    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();

        include dirname(__FILE__) . '/advanced-heading-view.php';

    }
}
