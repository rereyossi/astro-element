<?php
namespace Astro_Element;

class Activation
{

    public $secret_key = '5c0a2dd7949698.97710041s';
    public $server_host = 'https://webforia.id/';

    public function __construct()
    {
        add_action('admin_notices', array($this, 'check_license'));
    }

    public function check_license()
    {


        if (!empty($_POST['astro_element_license_submit']) && $_POST['astro_element_license_submit'] == 'Activate License') {
            $this->activation($_POST['astro_element_license_key']);

        }

        if (!empty($_POST['astro_element_license_submit']) && $_POST['astro_element_license_submit'] == 'Deactivate License') {
            $this->deactivation($_POST['astro_element_license_key']);
        }

    }

     /**
     * check product regiter for this license key
     *
     * @param [type] $key
     * @return string
     */
    public function get_product($key)
    {

         $api_params = array(
            'slm_action' => 'slm_check',
            'secret_key' => $this->secret_key,
            'license_key' => $key,
        );
        // Send query to the license manager server
        $response = wp_remote_get(add_query_arg($api_params, $this->server_host), array('timeout' => 20, 'sslverify' => false));

        $license_data = json_decode(wp_remote_retrieve_body($response));

        $product_ref = explode(' - ', $license_data->product_ref);

        // cek variation
        $product = !empty($product_ref[0]) ? $product_ref[0] : $license_data->product_ref;

        return str_replace(" ", "-", strtolower($product));

    }


    public function activation($key)
    {
        
        $api_params = array(
            'slm_action' => 'slm_activate',
            'secret_key' => $this->secret_key,
            'license_key' => $key,
            'registered_domain' => str_replace(array('www.', 'http://', 'https://'), '', site_url()),
        );

        // Send query to the license manager server
        $query = esc_url_raw(add_query_arg($api_params, $this->server_host));
        $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));

        // Check for error in the response
        if (is_wp_error($response)) {
            echo "Unexpected Error! The query returned with an error.";
        }

        // License data.
        $license_data = json_decode(wp_remote_retrieve_body($response));

        if ($license_data->result == 'success') { //Success was returned for the license activation

            if($this->get_product($key) == ASTRO_ELEMENT_SLUG){
                update_option(ASTRO_ELEMENT_SLUG.'_status', 'active');
                update_option(ASTRO_ELEMENT_SLUG.'_key', $key);
                Notice::get_success_notice($license_data->message);
            }else{
                $this->deactivation($key);
                 Notice::get_error_notice('Product not register for this license key');
            }
            

        } else {
            //Show error to the user. Probably entered incorrect license key.
            update_option(ASTRO_ELEMENT_SLUG . '_status', 'deactive');
            update_option(ASTRO_ELEMENT_SLUG . '_key', '');

            Notice::get_error_notice($license_data->message);
        }

    }

    public function deactivation($key)
    {

        // API query parameters
        $api_params = array(
            'slm_action' => 'slm_deactivate',
            'secret_key' => $this->secret_key,
            'license_key' => $key,
            'registered_domain' => str_replace(array('www.', 'http://', 'https://'), '', site_url()),
        );

        // Send query to the license manager server
        $query = esc_url_raw(add_query_arg($api_params, $this->server_host));
        $response = wp_remote_get($query, array('timeout' => 20, 'sslverify' => false));

        // Check for error in the response
        if (is_wp_error($response)) {
            echo "Unexpected Error! The query returned with an error.";
        }

        // License data.
        $license_data = json_decode(wp_remote_retrieve_body($response));

        if ($license_data->result == 'success') { //Success was returned for the license activation
            update_option(ASTRO_ELEMENT_SLUG.'_status', 'deactive');
            update_option(ASTRO_ELEMENT_SLUG.'_key', '');
            //Uncomment the followng line to see the message that returned from the license server
            Notice::get_success_notice($license_data->message);

        } else {
            //Show error to the user. Probably entered incorrect license key.
            Notice::get_error_notice($license_data->message);

        }

    }

    /**
     * Check status on option
     */

    public function get_status()
    {
        
        return get_option(ASTRO_ELEMENT_SLUG.'_status');

    }

    public static function active()
    {
        return (get_option(ASTRO_ELEMENT_SLUG.'_status') === 'active') ? true : false;
    }


  

}
new Activation;
