<?php 
$btn_primary_target = $data->button_primary_link['is_external'] ? ' target="_blank"' : '';
$btn_primary_nofollow = $data->button_primary_link['nofollow'] ? ' rel="nofollow"' : '';
$btn_second_target = $data->button_second_link['is_external'] ? ' target="_blank"' : '';
$btn_second_nofollow = $data->button_second_link['nofollow'] ? ' rel="nofollow"' : '';
?>

<div class="ael-cta">
	<div class="ael-cta__inner">

		<?php if (!empty($data->overtext)): ?>
		<span class="ael-cta__overtext"><?php echo $data->overtext ?></span>
		<?php endif?>

		<?php if(!empty($data->title)): ?>
		<h3 class="ael-cta__title"><?php echo $data->title ?></h3>
		<?php endif ?>

		<?php if(!empty($data->subtitle)): ?>
		<div class="ael-cta__subtitle"><?php echo $data->subtitle ?></div>
		<?php endif ?>

		<?php if(!empty($data->content)): ?>
		<div class="ael-cta__content"><?php echo $data->content ?></div>
		<?php endif ?>

		<div class="ael-cta__button">

            <?php if(!empty($data->button_second)): ?>
                <a class="rt-btn rt-btn--second" href="<?php echo $data->button_second_link['url']?>" <?php echo  $btn_second_target . $btn_second_nofollow?>><?php echo $data->button_second?></a>
            <?php endif ?>

			 <?php if (!empty($data->button_primary)): ?>
                <a class="rt-btn rt-btn--primary" href="<?php echo $data->button_primary_link['url'] ?>" <?php echo $btn_primary_target . $btn_primary_nofollow ?>><?php echo $data->button_primary ?></a>
            <?php endif?>
                
        </div>

	</div>
</div>