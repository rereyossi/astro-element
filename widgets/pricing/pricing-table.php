<?php
namespace Astro_Element\Elementor;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Pricing_Table extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-pricing-table';
    }

    public function get_title()
    {
        return __('Pricing Table', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-price';
    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    public function setting_header()
    {
        $this->start_controls_section(
            'setting_header',
            [
                'label' => __('Header', 'astro-element'),
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => __('Title', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Enter your title', 'astro-element'),
            ]
        );

        $this->add_control(
            'subtitle',
            [
                'label' => __('Sub Title', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Enter your description', 'astro-element'),
            ]
        );

        $this->end_controls_section();

    }

    public function setting_pricing()
    {
        $this->start_controls_section(
            'setting_pricing',
            [
                'label' => __('Pricing', 'astro-element'),
            ]
        );

        $this->add_control(
            'currency',
            [
                'label' => __('Currency Symbol', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '' => __('None', 'elementor-pro'),
                    'dollar' => '&#36; ' . _x('Dollar', 'Currency Symbol', 'elementor-pro'),
                    'euro' => '&#128; ' . _x('Euro', 'Currency Symbol', 'elementor-pro'),
                    'baht' => '&#3647; ' . _x('Baht', 'Currency Symbol', 'elementor-pro'),
                    'franc' => '&#8355; ' . _x('Franc', 'Currency Symbol', 'elementor-pro'),
                    'guilder' => '&fnof; ' . _x('Guilder', 'Currency Symbol', 'elementor-pro'),
                    'krona' => 'kr ' . _x('Krona', 'Currency Symbol', 'elementor-pro'),
                    'lira' => '&#8356; ' . _x('Lira', 'Currency Symbol', 'elementor-pro'),
                    'peseta' => '&#8359 ' . _x('Peseta', 'Currency Symbol', 'elementor-pro'),
                    'peso' => '&#8369; ' . _x('Peso', 'Currency Symbol', 'elementor-pro'),
                    'pound' => '&#163; ' . _x('Pound Sterling', 'Currency Symbol', 'elementor-pro'),
                    'real' => 'R$ ' . _x('Real', 'Currency Symbol', 'elementor-pro'),
                    'ruble' => '&#8381; ' . _x('Ruble', 'Currency Symbol', 'elementor-pro'),
                    'rupee' => '&#8360; ' . _x('Rupee', 'Currency Symbol', 'elementor-pro'),
                    'indian_rupee' => '&#8377; ' . _x('Rupee (Indian)', 'Currency Symbol', 'elementor-pro'),
                    'shekel' => '&#8362; ' . _x('Shekel', 'Currency Symbol', 'elementor-pro'),
                    'yen' => '&#165; ' . _x('Yen/Yuan', 'Currency Symbol', 'elementor-pro'),
                    'won' => '&#8361; ' . _x('Won', 'Currency Symbol', 'elementor-pro'),
                    'custom' => __('Custom', 'elementor-pro'),
                ],
                'default' => 'dollar',
            ]
        );

        $this->add_control(
            'currency_custom',
            [
                'label' => __('Custom Symbol', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'condition' => [
                    'currency' => 'custom',
                ],
            ]
        );

        $this->add_control(
            'price',
            [
                'label' => __('Price', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('10,00', 'astro-element'),
                'placeholder' => __('10,00', 'astro-element'),
            ]
        );
        $this->add_control(
            'period',
            [
                'label' => __('Period', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Monthly', 'astro-element'),
                'placeholder' => __('Monthly', 'astro-element'),
            ]
        );

        $this->add_control(
            'sale',
            [
                'label' => __('Sale', 'astro-element'),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => __('On', 'elementor-pro'),
                'label_off' => __('Off', 'elementor-pro'),
                'default' => '',
            ]
        );

        $this->add_control(
            'sale_price',
            [
                'label' => __('Sale Price', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'condition' => [
                    'sale' => 'yes',
                ],
            ]
        );

        $this->end_controls_section();

    }

    public function setting_featured()
    {
        $this->start_controls_section(
            'setting_featured',
            [
                'label' => __('Features', 'astro-element'),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'item', [
                'label' => __('Featured Item', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => __('Featured Item', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'icon', [
                'label' => __('Icon', 'astro-element'),
                'type' => Controls_Manager::ICON,
                'default' => __('fa fa-check', 'astro-element'),
            ]
        );

        $repeater->add_control(
            'disable', [
                'label' => __('Disable', 'astro-element'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'astro-element'),
                'label_off' => __('Off', 'astro-element'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'featured',
            [
                'label' => __('Featured', 'astro-element'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'item' => __('Featured Item', 'astro-element'),
                    ],
                ],
                'title_field' => '{{{ item }}}',
            ]
        );

        $this->end_controls_section();

    }

    public function setting_footer()
    {
        $this->start_controls_section(
            'setting_footer',
            [
                'label' => __('Footer', 'astro-element'),
            ]
        );

        $this->add_control(
            'footer_text',
            [
                'label' => __('Text', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Buy Now', 'astro-element'),
                'placeholder' => __('Buy Now', 'astro-element'),
            ]
        );

        $this->add_control(
            'footer_info',
            [
                'label' => __('Additional Info', 'astro-element'),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'footer_url',
            [
                'label' => __('URL Action', 'astro-element'),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => 'http://',
                    'is_external' => '',
                ],
                'show_external' => true,
            ]
        );
        $this->end_controls_section();
    }

    public function setting_ribbon()
    {
        $this->start_controls_section(
            'setting_ribbon',
            [
                'label' => __('Ribbon', 'elementor-pro'),
            ]
        );

        $this->add_control(
            'ribbon',
            [
                'label' => __('Show', 'elementor-pro'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', 'astro-element'),
                'label_on' => __('On', 'astro-element'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'ribbon_title',
            [
                'label' => __('Title', 'elementor-pro'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Popular', 'elementor-pro'),
                'condition' => [
                    'ribbon' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'ribbon_horizontal_position',
            [
                'label' => __('Horizontal Position', 'elementor-pro'),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => false,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'elementor-pro'),
                        'icon' => 'eicon-h-align-left',
                    ],
                    'right' => [
                        'title' => __('Right', 'elementor-pro'),
                        'icon' => 'eicon-h-align-right',
                    ],
                ],
                'condition' => [
                    'ribbon' => 'yes',
                ],
            ]
        );

        $this->end_controls_section();
    }

    public function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'general_border',
                'selector' => '{{WRAPPER}} .ael-price-table',

            ]
        );

        $this->add_responsive_control(
            'general_radius',
            [
                'label' => __('Border Radius', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table' => 'border-radius: {{SIZE}}{{UNIT}}; overflow: hidden;',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'general_shadow',
                'exclude' => [
                    'box_shadow_position',
                ],
                'selector' => '{{WRAPPER}} .ael-price-table',
            ]
        );

        $this->end_controls_section();

    }

    public function style_header()
    {
        $this->start_controls_section(
            'style_header',
            [
                'label' => __('Header', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'header_background',
                'selector' => '{{WRAPPER}} .ael-price-table__header',
            ]
        );

        $this->add_control(
            'style_header_title',
            [
                'label' => __('Title', 'elementor'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'heading_title_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'heading_title_typography',
                'selector' => '{{WRAPPER}} .ael-price-table__title',
            ]
        );
        $this->add_control(
            'style_header_sub_title',
            [
                'label' => __('Sub Title', 'elementor'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );
        $this->add_control(
            'heading_sub_title_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__sub-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'heading_sub_title_typography',
                'selector' => '{{WRAPPER}} .ael-price-table__sub-title',
            ]
        );

        $this->add_responsive_control(
            'general_padding',
            [
                'label' => __('Padding', 'astro-element'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__header' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();

    }

    public function style_pricing()
    {
        $this->start_controls_section(
            'style_price',
            [
                'label' => __('Price', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'price_background',
                'selector' => '{{WRAPPER}} .ael-price-table__price-wrapper',
            ]
        );

        $this->add_control(
            'price_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__price' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'price_typography',
                'selector' => '{{WRAPPER}} .ael-price-table__price',
            ]
        );
        $this->add_control(
            'price_original_header',
            [
                'label' => __('Original Price', 'astro-element'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'price_original_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__original-price' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'price_original_typography',
                'selector' => '{{WRAPPER}} .ael-price-table__original-price',
            ]
        );

        $this->add_control(
            'price_currency_header',
            [
                'label' => __('Currency Symbol', 'astro-element'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'price_currency_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__currency' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'price_currency_typography',
                'selector' => '{{WRAPPER}} .ael-price-table__currency',
            ]
        );

        $this->add_control(
            'price_period_header',
            [
                'label' => __('Period', 'astro-element'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'price_period_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__period' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'price_period_typography',
                'selector' => '{{WRAPPER}} .ael-price-table__period',
            ]
        );

        $this->add_control(
            'price_part_header',
            [
                'label' => __('Fractional Part', 'astro-element'),
                'type' => Controls_Manager::HEADING,
                'separator' => 'before',
            ]
        );

        $this->add_control(
            'price_part_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__after-price' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'price_part_typography',
                'selector' => '{{WRAPPER}} .ael-price-table__after-price',
            ]
        );

        $this->end_controls_section();

    }

    public function style_featured()
    {
        $this->start_controls_section(
            'style_featured',
            [
                'label' => __('Featured', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'featured_background',
                'selector' => '{{WRAPPER}} .ael-price-table__featured',
            ]
        );
        $this->add_control(
            'featured_color',
            [
                'label' => __('Color Active', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__featured li' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'featured_color_disable',
            [
                'label' => __('Color Disable', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__featured .is-disable' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'featured_icon_color',
            [
                'label' => __('Icon Active', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__featured li i' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'featured_icon_color_disable',
            [
                'label' => __('Icon Disable', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__featured .is-disable i' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'featured_typography',
                'selector' => '{{WRAPPER}} .ael-price-table__featured',
            ]
        );
        $this->add_responsive_control(
            'featured_gap',
            [
                'label' => __('Item Gap', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__featured li' => 'padding-top: {{SIZE}}{{UNIT}}; padding-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->add_control(
            'featured_divender_color',
            [
                'label' => __('Divender Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__featured li' => 'border-color: {{VALUE}} !important;',
                ],
            ]
        );

        $this->end_controls_section();

    }

    public function style_ribbon()
    {
        $this->start_controls_section(
            'style_ribbon',
            [
                'label' => __('Ribbon', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'ribbon_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__ribbon-inner' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'ribbon_typography',
                'selector' => '{{WRAPPER}} .ael-price-table__ribbon-inner',
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'ribbon_background',
                'selector' => '{{WRAPPER}} .ael-price-table__ribbon-inner',
            ]
        );

        $this->end_controls_section();

    }

    public function style_footer()
    {
        $this->start_controls_section(
            'style_footer',
            [
                'label' => __('Footer', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'footer_background',
                'selector' => '{{WRAPPER}} .ael-price-table__footer',
            ]
        );
        $this->add_control(
            'footer_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-price-table__footer' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'footer_typography',
                'selector' => '{{WRAPPER}} .ael-price-table__footer',
            ]
        );
        $this->end_controls_section();

    }

    protected function _register_controls()
    {
        $this->setting_header();
        $this->setting_pricing();
        $this->setting_featured();
        $this->setting_footer();
        $this->setting_ribbon();

        $this->style_general();
        $this->style_header();
        $this->style_pricing();
        $this->style_ribbon();
        $this->style_featured();
        $this->style_footer();

        $this->setting_button(array(
            'name' => 'readmore',
            'class' => '.ael-price-table__btn',
            'label' => 'Action Button',
        ));
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        // number
        $price = !empty($settings['sale']) ? $settings['sale_price'] : $settings['price'];
        $price_explode = explode('.', $price);
        $price_number = !empty($price_explode[0]) ? $price_explode[0] : '';
        $after_price = !empty($price_explode[1]) ? $price_explode[1] : '';

        include dirname(__FILE__) . '/pricing-table-view.php';
    }
}
