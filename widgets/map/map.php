<?php
namespace Astro_Element\Elementor;

use Elementor\Controls_Manager;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Map extends \Astro_Element\Elementor_Base
{

    public function get_name()
    {
        return 'astro-map';
    }

    public function get_title()
    {
        return __('Maps', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-map';
    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    protected function _register_controls()
    {

        /*=================================================
         *  SETTING: Map
        /*================================================= */
        $this->start_controls_section(
            'setting_map',
            [
                'label' => __('Maps Setting', 'astro-element'),
            ]
        );
        $this->add_control(
            'lat',
            [
                'label'   => __('Latitude Area', 'astro-element'),
                'default' => '-7.797973',
                'type'    => Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'lng',
            [
                'label'   => __('Longitude Area', 'astro-element'),
                'default' => '110.3464658',
                'type'    => Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'map_zoom',
            [
                'label'   => __('Zoom', 'astro-element'),
                'type'    => Controls_Manager::NUMBER,
                'default' => 12,
                'min'     => 1,
                'max'     => 30,
                'step'    => 1,
            ]
        );

        $this->add_responsive_control(
            'map_height',
            [
                'label'      => __('Height', 'astro-element'),
                'type'       => Controls_Manager::SLIDER,
                'default'    => [
                    'size' => 400,
                ],
                'range'      => [
                    'px' => [
                        'min'  => 0,
                        'max'  => 700,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%', 'vh'],
                'selectors'  => [
                    '{{WRAPPER}} .ael-map' => 'height: {{SIZE}}{{UNIT}}!important;',
                ],
            ]
        );

        $this->end_controls_section();

        /*=================================================
         *  MAKER
        /*================================================= */
        $this->start_controls_section(
            'setting_maker',
            [
                'label' => __('Maker', 'astro-element'),
            ]
        );

        $this->add_control(
            'maker',
            [
                'label'       => 'Maker',
                'type'        => Controls_Manager::REPEATER,
                'fields'      => [
                    [
                        'name'        => 'name',
                        'label'       => __('Name', 'astro-element'),
                        'type'        => Controls_Manager::TEXT,
                        'label_block' => true,
                        'default'     => __('Add Maker', 'astro-element'),
                    ],

                    [
                        'name'    => 'lat',
                        'label'   => __('Latitude', 'astro-element'),
                        'type'    => Controls_Manager::TEXT,
                        'default' => '-7.797973',
                    ],
                    [
                        'name'    => 'lng',
                        'label'   => __('Longitude', 'astro-element'),
                        'type'    => Controls_Manager::TEXT,
                        'default' => '110.3464658',
                    ],

                ],
                'title_field' => '{{{ name }}}',
            ]
        );

        $this->end_controls_section();

        /*=================================================
         *  SNAZZYMAPS
        /*================================================= */
        $this->start_controls_section(
            'setting_snazzymaps',
            [
                'label' => __('Snazzymaps', 'astro-element'),
            ]
        );

        $this->add_control(
            'setting_snazzymaps_choose',
            [
                'label'   => __('Map Style', 'astro-element'),
                'type'    => Controls_Manager::SELECT,
                'default' => 'default',
                'options' => [
                    'default'    => __('Default', 'astro-element'),
                    'grey'       => __('Grey', 'astro-element'),
                    'grey-light' => __('Grey Light', 'astro-element'),
                    'dark'       => __('Dark', 'astro-element'),
                    'dark-blue'  => __('Light Blue', 'astro-element'),
                    'custom'     => __('Custom', 'astro-element'),
                ],
            ]
        );

        $this->add_control(
            'setting_snazzymaps_custom',
            [
                'label'     => __('Snazzymaps', 'astro-element'),
                'type'      => Controls_Manager::CODE,
                'language'  => 'json',
                'condition' => [
                    'setting_snazzymaps_choose' => 'custom',
                ],
            ]
        );
        $this->end_controls_section();
    }

    protected function render()
    {
        $snazzymaps = '';
        $settings = $this->get_settings_for_display();

        if ($settings['setting_snazzymaps_choose'] == 'grey') {
            $snazzymaps = '[{"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":"50"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},{"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"lightness":-25},{"saturation":-100}]}]';
        } elseif ($settings['setting_snazzymaps_choose'] == 'grey-light') {
            $snazzymaps = '[{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]';
        } elseif ($settings['setting_snazzymaps_choose'] == 'dark') {
            $snazzymaps = '[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}]';
        } elseif ($settings['setting_snazzymaps_choose'] == 'dark-blue') {
            $snazzymaps = '[{"featureType":"water","elementType":"geometry","stylers":[{"color":"#193341"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#2c5a71"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#29768a"},{"lightness":-37}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#406d80"}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#3e606f"},{"weight":2},{"gamma":0.84}]},{"elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"weight":0.6},{"color":"#1a3541"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#2c5a71"}]}]';
        } elseif ($settings['setting_snazzymaps_choose'] == 'custom') {
            $snazzymaps = $settings['setting_snazzymaps_custom'];
        }

        
        include dirname(__FILE__) . '/map-view.php';

    }

}
