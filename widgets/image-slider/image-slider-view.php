<?php
$link_target = $slider['link']['is_external'] ? ' target="_blank"' : '';
$link_nofollow = $slider['link']['nofollow'] ? ' rel="nofollow"' : '';
?>
<?php if (!empty($slider['link']['url'])): ?>
<a href="<?php echo $slider['link']['url'] ?>" <?php echo $link_target . $link_nofollow ?>>
<?php endif?>

		<?php if (wp_is_mobile() && $slider['image_mobile']['id']): ?>

	        <?php if ($slider['image_mobile']['id']): ?>
	             <?php echo wp_get_attachment_image($slider['image_mobile']['id'], $settings['image_size']); ?>
	        <?php else: ?>
	            <img src="<?php echo $slider['image']['url'] ?>">
	        <?php endif?>

        <?php else: ?>

        	 <?php if ($slider['image']['id']): ?>
	             <?php echo wp_get_attachment_image($slider['image']['id'], $settings['image_size']); ?>
	        <?php else: ?>
	            <img src="<?php echo $slider['image']['url'] ?>">
	        <?php endif?>

	    <?php endif?>

<?php if (!empty($slider['link']['url'])): ?>
</a>
<?php endif?>

