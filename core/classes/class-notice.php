<?php
namespace Astro_Element;

class Notice
{

    public static function get_error_notice($message = '')
    {

        $message = !empty($message) ? $message : 'error notice';

        $output = "<div class='notice notice-warning is-dismissible'>";
        $output .= $message;
        $output .= "<button type='button' class='notice-dismiss'></button>";
        $output .= "</div>";

        echo $output;

    }

    public static function get_success_notice($message = '', $dismiss = '')
    {

        if (get_option($dismiss) === 'active' || empty(get_option($dismiss))) {

            $message = !empty($message) ? $message : 'success notice';

            $output = '<div class="notice-success notice is-dismissible">';
            $output .= $message;
            // $output .= '<a class="notice-dismiss"  href="' . wp_nonce_url(admin_url('?' . $dismiss . '=true')) . '">dismiss</a>';
            $output .= '</div>';

            echo $output;
        }

    }

    /**
     * update dismiss option
     * this function remove notice forever
     *
     * @param string $dismisss
     * @return void
     */
    public static function update_notice_option($dismiss = '')
    {
        $notif = !empty($_GET[$dismiss]) ? $_GET[$dismiss] : '';

        if (get_option($dismiss) === 'active') {
            update_option($dismiss, 'active');
        } else {
            update_option($dismiss, 'deactive');
        }
    }

}

new Notice;
