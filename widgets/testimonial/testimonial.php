<?php

namespace Astro_Element\Elementor;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Background;


use Astro_Element\Elementor_Base;
use Astro_Element\Helper;
use Astro_Element\HTML;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Testimonial extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-testimonial';
    }

    public function get_title()
    {
        return __('Testimonial', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-testimonial';
    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_option();
        $this->setting_content();
        
        $this->style_general();
        $this->style_name();
        $this->style_position();
        $this->style_content();

        $this->setting_carousel();

    }

    protected function setting_option()
    {
        $this->start_controls_section(
            'content_option',
            [
                'label' => __('Options', 'astro-element'),
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => __(1, 'astro-element'),
                    2 => __(2, 'astro-element'),
                    3 => __(3, 'astro-element'),
                    4 => __(4, 'astro-element'),
                    6 => __(6, 'astro-element'),
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );

        $this->add_control(
            'layout_masonry',
            [
                'label' => __('Masonry', 'astro-element'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', 'astro-element'),
                'label_on' => __('On', 'astro-element'),
                'return_value' => 'yes',
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

         $this->add_responsive_control(
            'setting_style',
            [
                'label' => __('Style', 'astro-element'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'tooltip' => __('Tooltip', 'astro-element'),
                    'card' => __('Card', 'astro-element'),
                ],
                'default' => 'card',
            ]
        );
        

        $this->end_controls_section();
    }

    public function setting_content()
    {
        $this->start_controls_section(
            'setting_content',
            [
                'label' => __('Content', 'astro-element'),
            ]
        );
        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'content', [
                'label' => __('Content', 'astro-element'),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $repeater->add_control(
            'image',
            [
                'label' => __('Choose Image', 'astro-element'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => \Elementor\Utils::get_placeholder_image_src(),
                ],
            ]
        );
        $repeater->add_control(
            'name', [
                'label' => __('Name', 'astro-element'),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $repeater->add_control(
            'position', [
                'label' => __('Position', 'astro-element'),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'testimonial',
            [
                'label' => __('Testimonial', 'astro-element'),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'name' => __('Jont Doe', 'astro-element'),
                        'position' => __('CEO', 'astro-element'),
                        'content' => __('I am slide content. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'astro-element'),
                    ],
                    [
                        'name' => __('Jont Doe', 'astro-element'),
                        'position' => __('CEO', 'astro-element'),
                        'content' => __('I am slide content. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'astro-element'),
                    ],
                    [
                        'name' => __('Jont Doe', 'astro-element'),
                        'position' => __('CEO', 'astro-element'),
                        'content' => __('I am slide content. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.', 'astro-element'),
                    ],

                ],
                'title_field' => '{{{ name }}}',
            ]
        );

        $this->end_controls_section();

    }

    public function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .flex-loop' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .flex-loop > .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                    padding-right: calc({{SIZE}}{{UNIT}}/2);
                                                    margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'general_background',
            [
                'label' => __('Background Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial--card' => 'background-color: {{VALUE}};',
                    '{{WRAPPER}} .ael-testimonial--card' => 'background-color: {{VALUE}};',
                ],
                'condition' => [
                    'setting_style' => 'card',
                ],
            ]
        );

        $this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'                  => 'general_border',
				'label'                 => __( 'Border', 'astro_domain' ),
				'placeholder'           => '1px',
				'default'               => '1px',
                'selector'              => '{{WRAPPER}} .ael-testimonial--card',
                'condition' => [
                    'setting_style' => 'card',
                ],
			]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'general_shadow',
                'selector' => '{{WRAPPER}} .ael-testimonial--card',
                 'condition' => [
                    'setting_style' => 'card',
                ],

            ]
        );

        $this->add_responsive_control(
            'general_radius',
            [
                'label' => __('Border Radius', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 100,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial--card' => 'border-radius: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'setting_style' => 'card',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_name()
    {
        $this->start_controls_section(
            'style_name',
            [
                'label' => __('Name', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'name_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial__name' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'name_typography',
                'selector' => '{{WRAPPER}} .ael-testimonial__name',
            ]
        );

        $this->add_responsive_control(
            'name_spacing',
            [
                'label' => __('Name Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial__name' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_position()
    {
        $this->start_controls_section(
            'style_position',
            [
                'label' => __('Position', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'position_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial__position' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'position_typography',
                'selector' => '{{WRAPPER}} .ael-testimonial__position',
            ]
        );

        $this->add_responsive_control(
            'position_spacing',
            [
                'label' => __('Position Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial__position' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_content()
    {
        $this->start_controls_section(
            'style_content',
            [
                'label' => __('Content', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'content_background',
            [
                'label' => __('Background Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial--tooltip .ael-testimonial__content' => 'background-color: {{VALUE}};border-color: {{VALUE}};',
                    '{{WRAPPER}} .ael-testimonial--tooltip .ael-testimonial__content:before' => 'background-color: {{VALUE}};border-color: {{VALUE}};',
                ],
                'condition' => [
                    'setting_style' => 'tooltip',
                ],
            ]
        );

        $this->add_control(
            'content_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'selector' => '{{WRAPPER}} .ael-testimonial__content',
            ]
        );

        $this->add_responsive_control(
            'content_spacing',
            [
                'label' => __('Position Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

         $this->add_responsive_control(
            'content_alignment',
            [
                'label' => __('Alignment', 'astro-element'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'astro-element'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'astro-element'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'astro-element'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .ael-testimonial__content' => 'text-align: {{VALUE}};',
                ],
            ]
        );
    

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings_for_display();

        $this->element_before_loop($settings, $this->get_id());

        foreach ($settings['testimonial'] as $key => $testimonial) {
            include dirname(__FILE__) . '/testimonial-view.php';
        }

        $this->element_after_loop($settings);

    }
}
