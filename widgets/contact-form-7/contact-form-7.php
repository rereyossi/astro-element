<?php
namespace Astro_Element\Elementor;

use Astro_Element\Helper;
use Astro_Element\HTML;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Contact_Form_7 extends  \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-contact-form-7';
    }

    public function get_title()
    {
        return __('Contact Form 7', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-contact';

    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    /**
     * Get Contact Form 7 if exits
     */
     public function contact_form_list(){
        $wpcf7_form_list = get_posts(array(
            'post_type' => 'wpcf7_contact_form',
            'showposts' => 999,
        ));
        $options = array();
        $options[0] = esc_html__( 'Select a Contact Form', 'astro_domain' );
        if ( ! empty( $wpcf7_form_list ) && ! is_wp_error( $wpcf7_form_list ) ){
            foreach ( $wpcf7_form_list as $post ) {
                $options[ $post->ID ] = $post->post_title;
            }
        } else {
            $options[0] = esc_html__( 'Create a Form First', 'astro_domain' );
        }
        return $options;
    }


    protected function _register_controls()
    {
        $this->setting_header_block();

        $this->setting_options(); //protected

        $this->style_input();
        $this->style_label();
        $this->style_placeholder();

        $this->setting_button(array(
            'class' => '.wpcf7-form input[type="submit"]',
            'label' => 'Submit Button',
        ));
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-element'),
            ]
        );


        $this->add_control(
			'contact_form_list',
			[
				'label'                 => esc_html__( 'Select Form', 'astro_domain' ),
				'type'                  => Controls_Manager::SELECT,
				'label_block'           => true,
				'options'               => $this->contact_form_list(),
                'default'               => '0',
			]
        );
        

        $this->end_controls_section();
    }


    public function style_input(){

           $this->start_controls_section(
            'style_input',
            [
                'label'                 => __( 'Input', 'astro_domain' ),
                'tab'                   => Controls_Manager::TAB_STYLE,
            ]
        );

         $this->start_controls_tabs( 'input_tabs' );
         $this->start_controls_tab(
            'input_normal',
            [
                'label'                 => __( 'Normal', 'astro_domain' ),
            ]
        );
        /** Input normal start*/
          $this->add_control(
            'input_background',
            [
                'label'                 => __( 'Background Color', 'astro_domain' ),
                'type'                  => Controls_Manager::COLOR,
                'selectors'             => [
                    '{{WRAPPER}} .wpcf7-form-control.wpcf7-text, 
                    {{WRAPPER}} .wpcf7-form-control.wpcf7-textarea, 
                    {{WRAPPER}} .wpcf7-form-control.wpcf7-select' => 'background-color: {{VALUE}}',
                ],
            ]
        );

        /** Input normal end */
         $this->end_controls_tab();
          $this->start_controls_tab(
            'input_focus',
            [
                'label'                 => __( 'Focus', 'astro_domain' ),
            ]
        );
         /** Input focus start */
        /** Input normal start*/
          $this->add_control(
            'input_background_focus',
            [
                'label'                 => __( 'Background Color', 'astro_domain' ),
                'type'                  => Controls_Manager::COLOR,
                'selectors'             => [
                    '{{WRAPPER}} .wpcf7-form-control.wpcf7-text:focus, 
                    {{WRAPPER}} .wpcf7-form-control.wpcf7-textarea:focus, 
                    {{WRAPPER}} .wpcf7-form-control.wpcf7-select:focus' => 'background-color: {{VALUE}}',
                ],
            ]
        );
        /** Input focus end */
        $this->end_controls_tab();
        $this->end_controls_tabs();

        /** Input tabs end */
        $this->add_responsive_control(
            'input_spacing',
            [
                'label'                 => __( 'Spacing', 'astro_domain' ),
                'type'                  => Controls_Manager::SLIDER,
                'range'                 => [
                    'px'        => [
                        'min'   => 0,
                        'max'   => 100,
                        'step'  => 1,
                    ],
                ],
                'size_units'            => [ 'px', '%' ],
                'selectors'             => [
                    '{{WRAPPER}} .wpcf7-form-control' => 'margin-bottom: {{SIZE}}{{UNIT}}',
                ],
                'separator'             => 'before',
            ]
        );

        $this->add_responsive_control(
			'input_padding',
			[
				'label'                 => __( 'Padding', 'astro_domain' ),
				'type'                  => Controls_Manager::DIMENSIONS,
				'size_units'            => [ 'px', '%' ],
				'selectors'             => [
                    '{{WRAPPER}} .wpcf7-form-control.wpcf7-text, 
                    {{WRAPPER}} .wpcf7-form-control.wpcf7-textarea' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
        );
        
        $this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'                  => 'input_border',
				'label'                 => __( 'Border', 'astro_domain' ),
				'placeholder'           => '1px',
				'default'               => '1px',
                'selector'              => '{{WRAPPER}} .wpcf7-form-control.wpcf7-text, 
                                            {{WRAPPER}} .wpcf7-form-control.wpcf7-textarea, 
                                            {{WRAPPER}} .wpcf7-form-control.wpcf7-select',
			]
        );
        
          $this->add_responsive_control(
            'input_radius',
            [
                'label'                 => __( 'Border Radius', 'astro_domain' ),
                'type'                  => Controls_Manager::SLIDER,
                'range'                 => [
                    'px'        => [
                        'min'   => 0,
                        'max'   => 30,
                        'step'  => 1,
                    ],
                ],
                'size_units'            => [ 'px','%' ],
                'selectors'             => [
                    '{{WRAPPER}} .wpcf7-form-control' => 'border-radius: {{SIZE}}{{UNIT}}',
                ],
            ]
        );

        $this->end_controls_section();

    }

    public function style_label(){
         $this->start_controls_section(
            'style_label',
            [
                'label'                 => __( 'Label', 'astro_domain' ),
                'tab'                   => Controls_Manager::TAB_STYLE,
            ]
        );
         $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'                  => 'label_typography',
                'label'                 => __( 'Typography', 'astro_domain' ),
                'selector'              => '{{WRAPPER}} .wpcf7-form label',
            ]
        );
        $this->add_control(
			'label_color',
			[
				'label' => esc_html__( 'Color', 'astro_domain' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpcf7-form label' => 'Color: {{VALUE}};',
				],
			]
        );

         $this->add_responsive_control(
            'label_spacing',
            [
                'label'                 => __( 'Spacing', 'astro_domain' ),
                'type'                  => Controls_Manager::SLIDER,
                'range'                 => [
                    'px'        => [
                        'min'   => 0,
                        'max'   => 100,
                        'step'  => 1,
                    ],
                ],
                'size_units'            => [ 'px', '%' ],
                'selectors'             => [
                    '{{WRAPPER}} .wpcf7-form .wpcf7-form-control' => 'margin-top: {{SIZE}}{{UNIT}}',
                ],
            ]
        );
        
        
        $this->end_controls_section();

    }

     public function style_placeholder(){
         $this->start_controls_section(
            'style_placeholder',
            [
                'label'                 => __( 'Placeholder', 'astro_domain' ),
                'tab'                   => Controls_Manager::TAB_STYLE,
            ]
        );
         $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name'                  => 'placeholder_typography',
                'label'                 => __( 'Typography', 'astro_domain' ),
                'selector'              => '{{WRAPPER}} .wpcf7-form-control::-webkit-input-placeholder',
            ]
        );
        $this->add_control(
			'placeholder_color',
			[
				'label' => esc_html__( 'Color', 'astro_domain' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .wpcf7-form-control::-webkit-input-placeholder' => 'Color: {{VALUE}};',
				],
			]
        );
        
        $this->end_controls_section();

    }



    protected function render()
    {
        $settings = $this->get_settings_for_display();
        
         // Header
        echo HTML::header_block(array(
            'class' => 'ael-header-block--' . $settings['header_style'],
            'title' => $settings['header_title'],
            'id' => 'header-' . $this->get_id(),
        ));
        
        echo '<div class="ael-cf7">';

        if ( function_exists( 'wpcf7' ) ) {
            if ( ! empty( $settings['contact_form_list'] ) ) {
                echo do_shortcode( '[contact-form-7 id="' . $settings['contact_form_list'] . '"  html_class="form contact-form"]' );
            }
        }else{
            echo __('Plugin Contact Form 7 not active','astro-element');
        }
        
        echo '</div>';
       
        
    }
}
