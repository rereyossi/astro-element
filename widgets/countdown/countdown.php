<?php
namespace Astro_Element\Elementor;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Countdown extends \Astro_Element\Elementor_Base
{
    public function get_name()
    {
        return 'astro-countdown';
    }

    public function get_title()
    {
        return __('Countdown', 'astro-element');
    }

    public function get_icon()
    {
        return 'ate-icon ate-countdown';

    }

    public function get_categories()
    {
        return ['astro-element'];
    }

    protected function _register_controls()
    {

        $this->setting_options(); //protected

        $this->syle_general();
        $this->style_number();
        $this->style_title();
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'astro-element'),
            ]
        );

        $this->add_control(
            'date',
            [
                'label' => __('Due Date', 'astro-element'),
                'type' => Controls_Manager::DATE_TIME,
                'default' => date('Y-m-d H:i'),
                'label_block' => 'yes',
            ]
        );

        $this->add_control(
            'day_label',
            [
                'label' => __('Days Label', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Days', 'astro-element'),
            ]
        );

        $this->add_control(
            'hour_label',
            [
                'label' => __('Hours Label', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Hours', 'astro-element'),
            ]
        );
        $this->add_control(
            'minute_label',
            [
                'label' => __('Minutes Label', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Minutes', 'astro-element'),
            ]
        );

        $this->add_control(
            'second_label',
            [
                'label' => __('Seconds Label', 'astro-element'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Seconds', 'astro-element'),
            ]
        );

        $this->end_controls_section();
    }

    public function syle_general()
    {

        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /** background tabs start */
        $this->start_controls_tabs('general_background_tabs');

        $this->start_controls_tab(
            'general_background_normal',
            [
                'label' => __('Normal', 'astro-element'),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'general_background',
                'selector' => '{{WRAPPER}} .ael-countdown__item',
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'general_border',
                'selector' => '{{WRAPPER}} .ael-countdown__item',
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'general_shadow',
                'selector' => '{{WRAPPER}} .ael-countdown__item',

            ]
        );

        $this->end_controls_tab();
        $this->start_controls_tab(
            'general_background_tab_hover',
            [
                'label' => __('Hover', 'astro-element'),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'general_background_hover',
                'selector' => '{{WRAPPER}} .ael-countdown__item:hover',
            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'general_border_hover',
                'selector' => '{{WRAPPER}} .ael-countdown__item:hover',
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'general_shadow_hover',
                'selector' => '{{WRAPPER}} .ael-countdown__item:hover',

            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

        /** background tabs end */

        $this->add_responsive_control(
            'general_spacing',
            [
                'label' => __('Space Between', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],

                'selectors' => [
                    '{{WRAPPER}} .ael-countdown__item' => 'margin-left: {{SIZE}}{{UNIT}}; margin-right: {{SIZE}}{{UNIT}}',
                ],
            ]
        );

        $this->add_responsive_control(
            'general_padding',
            [
                'label' => __('Padding', 'astro-element'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-countdown__item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'countdown_radius',
            [
                'label' => __('Border Radius', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-countdown__item' => 'border-radius: {{SIZE}}{{UNIT}}',
                ],
            ]
        );

        $this->end_controls_section();
    }

    public function style_title()
    {
        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,

            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'selector' => '{{WRAPPER}} .ael-countdown__title',
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-countdown__title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();
    }

    public function style_number()
    {
        $this->start_controls_section(
            'style_number',
            [
                'label' => __('Number', 'astro-element'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'number_color',
            [
                'label' => __('Color', 'astro-element'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ael-countdown__number' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'number_typography',
                'selector' => '{{WRAPPER}} .ael-countdown__number',
            ]
        );

        $this->add_responsive_control(
            'number_spacing',
            [
                'label' => __('Number Spacing', 'astro-element'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 1,
                        'max' => 50,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ael-countdown__number' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {

        $settings = $this->get_settings_for_display();

        include dirname(__FILE__) . '/countdown-view.php';

    }
}
